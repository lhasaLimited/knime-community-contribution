/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.util;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Samuel
 *
 */
public class TestBinner
{	
	private static Binner BINNER;
	private static double DELTA = 0.02d;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception
	{
		BINNER = new Binner(10, 0.2, -1, 1);
	}

	/**
	 * Test method for {@link org.lhasalimited.generic.util.Binner#getBinRanges()}.
	 */
	@Test
	public void testGetBinRanges()
	{
		String[] expected = new String[]
		{
				 "[-1.0 ... -0.8]", 
				 "]-0.8 ... -0.6]", 
				 "]-0.6 ... -0.4]", 
				 "]-0.4 ... -0.2]", 
				 "]-0.2 ... 0.0]", 
				 "]0.0 ... 0.2]", 
				 "]0.2 ... 0.4]", 
				 "]0.4 ... 0.6]", 
				 "]0.6 ... 0.8]", 
				 "]0.8 ... 1.0]"	
		};	

		String[] values = BINNER.getBinRanges();
		
		assertArrayEquals(expected, values);
	}

	/**
	 * Test method for {@link org.lhasalimited.generic.util.Binner#findBinIndex(double)}.
	 */
	@Test
	public void testFindBinIndex()
	{
	
		int index = BINNER.findBinIndex(-1.0);
		int indexTwo = BINNER.findBinIndex(0.0);
		int indexThree = BINNER.findBinIndex(1.0);
		int indexFour = BINNER.findBinIndex(0.22);
		
		int[] expected = new int[]{0, 4, 9, 6};
		int[] retrieved =  new int[]{index, indexTwo, indexThree, indexFour};
		
		assertArrayEquals(expected, retrieved);
		
	}
	
	@Test
	public void testOutOfBoundsValue()
	{
		try
		{
			BINNER.findBinIndex(1.8);
			fail("Exception not thrown");
		} catch (IllegalArgumentException e)
		{
			assertEquals(e.getMessage(), "The value given was greater than the last bin range");
		}

	}

	/**
	 * Test method for {@link org.lhasalimited.generic.util.Binner#getUpperBounds()}.
	 */
	@Test
	public void testGetUpperBounds()
	{
		double[] range = BINNER.getUpperBounds();
		double[] expected = new double[]{-0.8d, -0.6d, -0.4d, -0.2d, -0.0d, 0.2d, 0.4d, 0.6d, 0.8d, 1.0d};
		
		assertArrayEquals(expected, range, DELTA);
	}

	/**
	 * Test method for {@link org.lhasalimited.generic.util.Binner#getLowerBounds()}.
	 */
	@Test
	public void testGetLowerBounds()
	{
		double[] range = BINNER.getLowerBounds();
		double[] expected = new double[]{-1d, -0.8d, -0.6d, -0.4d, -0.2d, -0.0d, 0.2d, 0.4d, 0.6d, 0.8d};
		
		assertArrayEquals(expected, range, DELTA);
	}

}
