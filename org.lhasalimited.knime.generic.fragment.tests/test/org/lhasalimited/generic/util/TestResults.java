/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */

package org.lhasalimited.generic.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.Map;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Samuel
 * 
 */
public class TestResults
{

	private Results result;

	private int tp = 50;
	private int fp = 20;
	private int tn = 80;
	private int fn = 10;
	private int total = tp + fp + tn + fn;

	private static double DELTA = 0.02d;

	@Before
	public void setupResults()
	{
		result = new Results(tp, fp, tn, fn);
	}

	/**
	 * Checks that the calculation of the type is correct given a map
	 */
	@Test
	public void testMapTypeCalculation()
	{
		Map<Result, String> map = new TreeMap<Result, String>();
		map.put(Result.ACTIVE, "ACTIVE");
		map.put(Result.INACTIVE, "INACTIVE");
		map.put(Result.EQUIVOCAL, "EQUIVOCAL");
		map.put(Result.OUT_OF_DOMAIN, "OUT");

		Results resultTwo = new Results(map);

		boolean fpCorrect = resultTwo.calculateType("INACTIVE", "ACTIVE") == Result.FALSE_POSITIVE;
		boolean tpCorrect = resultTwo.calculateType("ACTIVE", "ACTIVE") == Result.TRUE_POSITIVE;
		boolean fnCorrect = resultTwo.calculateType("ACTIVE", "INACTIVE") == Result.FALSE_NEGATIVE;
		boolean tnCorrect = resultTwo.calculateType("INACTIVE", "INACTIVE") == Result.TRUE_NEGATIVE;
		boolean outCorrect = resultTwo.calculateType("ACTIVE", "OUT") == Result.OUT_OF_DOMAIN;
		boolean equivocalCorrect = resultTwo.calculateType("ACTIVE", "EQUIVOCAL") == Result.EQUIVOCAL;

		boolean allCorrect = fpCorrect & tpCorrect & fnCorrect & tnCorrect & outCorrect
				& equivocalCorrect;

		assertTrue(allCorrect);
	}
	
	@Test
	public void calculateType_shouldErrorWhenProvidingUnmappedValue()
	{
		Map<Result, String> map = new TreeMap<Result, String>();
		map.put(Result.ACTIVE, "ACTIVE");
		map.put(Result.INACTIVE, "INACTIVE");
		map.put(Result.EQUIVOCAL, "EQUIVOCAL");
		map.put(Result.OUT_OF_DOMAIN, "OUT");

		Results resultTwo = new Results(map);
		
		assertThrows(IllegalArgumentException.class, () -> {
			resultTwo.calculateType("INACTIVE", "unknownValueNotInTheMap");
		});
	}

	/**
	 * Test method for {@link org.lhasalimited.generic.util.Results#getTotal()}.
	 */
	@Test
	public void testGetTotal()
	{
		assertEquals(result.getTotal(), total);
	}

	/**
	 * Test method for
	 * {@link org.lhasalimited.generic.util.Results#getAccuracy()}.
	 */
	@Test
	public void testGetAccuracy()
	{
		assertEquals(result.getAccuracy(), 0.8125, DELTA);
	}

	/**
	 * Test method for
	 * {@link org.lhasalimited.generic.util.Results#getSensitivity()}.
	 */
	@Test
	public void testGetSensitivity()
	{
		assertEquals(result.getSensitivity(), 0.83333333, DELTA);
	}

	/**
	 * Test method for
	 * {@link org.lhasalimited.generic.util.Results#getSpecificity()}.
	 */
	@Test
	public void testGetSpecificity()
	{
		assertEquals(result.getSpecificity(), 0.8, DELTA);
	}

	/**
	 * Test method for
	 * {@link org.lhasalimited.generic.util.Results#getBalancedAccuracy()}.
	 */
	@Test
	public void testGetBalancedAccuracy()
	{
		assertEquals(result.getBalancedAccuracy(), 0.816666667, DELTA);
	}

	/**
	 * Test method for
	 * {@link org.lhasalimited.generic.util.Results#getPrecision()}.
	 */
	@Test
	public void testGetPrecision()
	{
		assertEquals(result.getPrecision(), 0.714285714, DELTA);
	}

	/**
	 * Test method for {@link org.lhasalimited.generic.util.Results#getRecall()}
	 * .
	 */
	@Test
	public void testGetRecall()
	{
		assertEquals(result.getBalancedAccuracy(), 0.833333333, DELTA);
	}

	/**
	 * Test method for
	 * {@link org.lhasalimited.generic.util.Results#getFMeasure()}.
	 */
	@Test
	public void testGetFMeasure()
	{
		assertEquals(result.getFMeasure(), 0.769230769, DELTA);
	}

	/**
	 * Test method for
	 * {@link org.lhasalimited.generic.util.Results#getNegativePredictivity()}.
	 */
	@Test
	public void testGetNegativePredictivity()
	{
		assertEquals(result.getNegativePredictivity(), 0.888888889, DELTA);
	}
	
	/**
	 * Test method for
	 * {@link org.lhasalimited.generic.util.Results#getBalancedMcc()}.
	 */
	@Test
	public void testGetBalancedMCC()
	{
		assertEquals(result.getBalancedMcc(), 0.63368547867, DELTA);
	}

}
