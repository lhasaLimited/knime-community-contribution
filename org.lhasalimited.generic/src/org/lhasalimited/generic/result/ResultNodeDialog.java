/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.result;

import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentString;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.util.DataValueColumnFilter;
import org.lhasalimited.generic.binnedperformance.BinnedPerformanceNodeModel;

/**
 * <code>NodeDialog</code> for the "Result" Node.
 * Returns the result of the prediction: true positive, true negative, false positive, false negative.
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Lhasa Limited
 */
public class ResultNodeDialog extends DefaultNodeSettingsPane 
{
	
    protected ResultNodeDialog() 
    {

    	createNewGroup("Columns");
    	setHorizontalPlacement(true);
    	addDialogComponent(new DialogComponentColumnNameSelection(new SettingsModelColumnName(ResultNodeModel.CONFIG_TARGET_COLUMN, "Target"), "Target:", 0, StringValue.class));
		addDialogComponent(new DialogComponentColumnNameSelection(new SettingsModelColumnName(ResultNodeModel.CONFIG_PREDICTION_COLUMN, "Prediction"), "Prediction:", 0, StringValue.class));	
		addDialogComponent(new DialogComponentString(new SettingsModelString(ResultNodeModel.CONFIG_COLUMN_NAME, "Result"), "Result column"));
		
		createNewGroup("Values");
		setHorizontalPlacement(false);
		setHorizontalPlacement(true);
		addDialogComponent(new DialogComponentString(new SettingsModelString(ResultNodeModel.CONFIG_POSITIVE_STRING, "active"), "Positive string:"));
		addDialogComponent(new DialogComponentString(new SettingsModelString(ResultNodeModel.CONFIG_NEGATIVE_STRING, "inactive"), "Negative string:"));
                    
    }
}

