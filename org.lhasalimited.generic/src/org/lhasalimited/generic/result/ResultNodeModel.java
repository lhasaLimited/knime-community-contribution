/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.result;

import java.io.File;
import java.io.IOException;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.MissingCell;
import org.knime.core.data.StringValue;
import org.knime.core.data.container.CellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.container.SingleCellFactory;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.streamable.simple.SimpleStreamableFunctionNodeModel;
import org.lhasalimited.generic.util.Result;

/**
 * This is the model implementation of Result. Returns the result of the
 * prediction: true positive, true negative, false positive, false negative.
 *
 * @author Lhasa Limited
 */
public class ResultNodeModel extends SimpleStreamableFunctionNodeModel
{

	static final String CONFIG_TARGET_COLUMN = "Activity column";
	private final SettingsModelColumnName settingTargetColumn = new SettingsModelColumnName(CONFIG_TARGET_COLUMN,
			"Target");

	static final String CONFIG_PREDICTION_COLUMN = "Prediction column";
	private final SettingsModelColumnName settingPredictionColumn = new SettingsModelColumnName(
			CONFIG_PREDICTION_COLUMN, "Prediction");

	static final String CONFIG_POSITIVE_STRING = "Active flag";
	private final SettingsModelString settingActiveString = new SettingsModelString(CONFIG_POSITIVE_STRING, "active");
	static final String CONFIG_NEGATIVE_STRING = "Inactive flag";
	private final SettingsModelString settingInactiveString = new SettingsModelString(CONFIG_NEGATIVE_STRING,
			"inactive");

	static final String CONFIG_COLUMN_NAME = "Output column name";
	private final SettingsModelString settingOutputColumnName = new SettingsModelString(CONFIG_COLUMN_NAME, "Result");

	// protected ResultNodeModel()
	// {
	// super(1, 1);
	// }

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec)
			throws Exception
	{

		ColumnRearranger rearranger = createColumnRearranger(inData[0].getDataTableSpec());
		BufferedDataTable out = exec.createColumnRearrangeTable(inData[0], rearranger, exec);

		return new BufferedDataTable[] { out };

	}

	/**
	 * Create the column rearranger. Sub methods will deal with the
	 * identification of TP, FP, TN, FN and unknown Could use the results class
	 * but would need to keep creating a Results object for the multi threading.
	 * 
	 * @param inSpec
	 *            The input table spec
	 * 
	 * @return The column rearanger to use.
	 */
	@Override
	protected ColumnRearranger createColumnRearranger(final DataTableSpec inSpec)
	{

		ColumnRearranger rearranger = new ColumnRearranger(inSpec);

		DataColumnSpec newColSpec = new DataColumnSpecCreator(
				inSpec.getUniqueColumnName(inSpec, settingOutputColumnName.getStringValue()), StringCell.TYPE)
						.createSpec();

		CellFactory factory = new SingleCellFactory(true, newColSpec)
		{

			@Override
			public DataCell getCell(DataRow row)
			{
				int targetIndex = inSpec.findColumnIndex(settingTargetColumn.getColumnName());
				int predictionIndex = inSpec.findColumnIndex(settingPredictionColumn.getColumnName());

				DataCell cell = null;

				if (row.getCell(targetIndex).isMissing() || row.getCell(predictionIndex).isMissing())
				{
					cell = new MissingCell("Cannot compare null cells");
				} else
				{
					cell = new StringCell(getResult(((StringValue) row.getCell(targetIndex)).getStringValue(),
							((StringValue) row.getCell(predictionIndex)).getStringValue()));
				}

				return cell;
			}

			/**
			 * 
			 * @param target
			 *            the target string value
			 * @param prediction
			 *            the prediction string value
			 * 
			 * @return
			 */
			private String getResult(String target, String prediction)
			{

				String result = Result.UNKNOWN.toString();

				if (target.equals(settingActiveString.getStringValue()))
				{
					if (prediction.equals(settingActiveString.getStringValue()))
					{
						result = Result.TRUE_POSITIVE.toString();

					} else if (prediction.equals(settingInactiveString.getStringValue()))
					{
						result = Result.FALSE_NEGATIVE.toString();
					}
					// else
					// {
					// result = Result.UNKNOWN.toString();
					// }

				} else if (target.equals(settingInactiveString.getStringValue()))
				{
					if (prediction.equals(settingActiveString.getStringValue()))
					{
						result = Result.FALSE_POSITIVE.toString();

					} else if (prediction.equals(settingInactiveString.getStringValue()))
					{
						result = Result.TRUE_NEGATIVE.toString();
					}
					// else
					// {
					// result = Result.UNKNOWN.toString();
					// }
				}
				// else
				// {
				// result = Result.UNKNOWN.toString();
				// }

				return result;
			}
		};

		rearranger.append(factory);

		return rearranger;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset()
	{
		// TODO Code executed on reset.
		// Models build during execute are cleared here.
		// Also data handled in load/saveInternals will be erased here.
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException
	{
		return new DataTableSpec[] { createColumnRearranger(inSpecs[0]).createSpec() };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings)
	{
		settingActiveString.saveSettingsTo(settings);
		settingInactiveString.saveSettingsTo(settings);
		settingPredictionColumn.saveSettingsTo(settings);
		settingOutputColumnName.saveSettingsTo(settings);
		settingTargetColumn.saveSettingsTo(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		settingActiveString.loadSettingsFrom(settings);
		settingInactiveString.loadSettingsFrom(settings);
		settingPredictionColumn.loadSettingsFrom(settings);
		settingOutputColumnName.loadSettingsFrom(settings);
		settingTargetColumn.loadSettingsFrom(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		settingActiveString.validateSettings(settings);
		settingInactiveString.validateSettings(settings);
		settingPredictionColumn.validateSettings(settings);
		settingOutputColumnName.validateSettings(settings);
		settingTargetColumn.validateSettings(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{

		// TODO load internal data.
		// Everything handed to output ports is loaded automatically (data
		// returned by the execute method, models loaded in loadModelContent,
		// and user settings set through loadSettingsFrom - is all taken care
		// of). Load here only the other internals that need to be restored
		// (e.g. data used by the views).

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{

		// TODO save internal models.
		// Everything written to output ports is saved automatically (data
		// returned by the execute method, models saved in the saveModelContent,
		// and user settings saved through saveSettingsTo - is all taken care
		// of). Save here only the other internals that need to be preserved
		// (e.g. data used by the views).

	}

}
