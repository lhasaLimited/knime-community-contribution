/**
* Copyright (C) 2015  Lhasa Limited
* 
* This program is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* These nodes extend the KNIME core which is free software developed
* by  KNIME Gmbh (http://www.knime.org/).
* 
*/
package org.lhasalimited.generic.htmltotable;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "TableToHtml" Node.
 * Convert a complete table to a single HTML cell
 *
 * @author Lhasa Limited
 * @deprecated
 */
public class TableToHtmlNodeView extends NodeView<TableToHtmlNodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link TableToHtmlNodeModel})
     */
    protected TableToHtmlNodeView(final TableToHtmlNodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

