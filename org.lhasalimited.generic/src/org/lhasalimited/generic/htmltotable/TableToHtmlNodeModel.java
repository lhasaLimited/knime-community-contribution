/**
* Copyright (C) 2015  Lhasa Limited
* 
* This program is free software: you can redistribute it and/or modify 
* it under the terms of the GNU General Public License as published by 
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
* 
* These nodes extend the KNIME core which is free software developed
* by  KNIME Gmbh (http://www.knime.org/).
* 
*/
package org.lhasalimited.generic.htmltotable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowKey;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.port.PortObjectSpec;
import org.knime.core.node.streamable.InputPortRole;
import org.knime.core.node.streamable.OutputPortRole;
import org.knime.core.node.streamable.PartitionInfo;
import org.knime.core.node.streamable.PortInput;
import org.knime.core.node.streamable.PortOutput;
import org.knime.core.node.streamable.RowInput;
import org.knime.core.node.streamable.RowOutput;
import org.knime.core.node.streamable.StreamableOperator;
import org.knime.core.node.streamable.StreamableOperatorInternals;
import org.lhasalimited.generic.htmltotable.util.TableToHtml;

/**
 * This is the model implementation of TableToHtml. Convert a complete table to
 * a single HTML cell
 * 
 * @author Lhasa Limited
 * @deprecated
 */
public class TableToHtmlNodeModel extends NodeModel
{
	protected static String CFG_COLUMN_SELECTION = "COLUMN_SELECTION";
	private SettingsModelFilterString settingColumnFilter = new SettingsModelFilterString(CFG_COLUMN_SELECTION);
	
	protected static String CFG_NUM_ROWS = "NUM_ROWS";
	private SettingsModelInteger settingNumRows = new SettingsModelInteger(CFG_NUM_ROWS, 1000);
	
	protected static String CFG_INLCUDE_ROWID = "INCLUDE_ROWID";
	private SettingsModelBoolean settingIncludeRowid = new SettingsModelBoolean(CFG_INLCUDE_ROWID, true);
	
	protected static String CFG_DOUBLE_FORMAT = "DOUBLE_FORMAT";
	private SettingsModelString settingsDoubleFormat = new SettingsModelString(CFG_DOUBLE_FORMAT, "#.00");
	
	protected static String CFG_FORMAT_DOUBLE = "FORMAT_DOUBLE";
	private SettingsModelBoolean settingsFormatDouble = new SettingsModelBoolean(CFG_FORMAT_DOUBLE, true);
	
	protected static String CFG_ADD_BASIC_STYLE = "ADD_BASIC_STYLE";
	private SettingsModelBoolean settingsAddBasicStyle = new SettingsModelBoolean(CFG_ADD_BASIC_STYLE, false);


	
	private TableToHtml streamConverter;
	private int streamTrack;
	
	/**
	 * Constructor for the node model.
	 */
	protected TableToHtmlNodeModel()
	{
		super(1, 1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec) throws Exception
	{	
		// Get the input table
		BufferedDataTable table = inData[0];

		// Create the table converter
		TableToHtml converter = new TableToHtml(settingNumRows.getIntValue(), 
												settingsFormatDouble.getBooleanValue() ? settingsDoubleFormat.getStringValue() : null, 
												settingIncludeRowid.getBooleanValue(), 
												settingsAddBasicStyle.getBooleanValue());
		
		List<String> included = getIncludedColumns(settingColumnFilter, table.getDataTableSpec());
		
		
		// Convert it to HTML
		String htmlTable;
		try 
		{
			htmlTable = converter.convertToHtml(table, included);
		} catch (Exception e) 
		{
			e.printStackTrace();
			throw new Exception("Failed to convert to HTML", e);
		}

		// Create the spec
		DataTableSpec outputSpec = createTableSpec();
		BufferedDataContainer container = exec.createDataContainer(outputSpec);

		// Add row to table
		container.addRowToTable(createRow(htmlTable));
		container.close();
		
		
//		FlowVariable variable = new FlowVariable("html-content", htmlTable);
		pushFlowVariableString("html-content", htmlTable);
		
		// Return the table
		return new BufferedDataTable[] { container.getTable() };
	}

	/**
	 * 
	 * @param htmlTable
	 * @return
	 */
	private DefaultRow createRow(String htmlTable)
	{
		DataCell[] cells = new DataCell[1];

		cells[0] = new StringCell(htmlTable);

		return new DefaultRow(new RowKey("HTML"), cells);
	}

	/**
	 * 
	 * @return
	 */
	private DataTableSpec createTableSpec()
	{
		DataColumnSpec[] allColSpecs = new DataColumnSpec[1];

		allColSpecs[0] = new DataColumnSpecCreator("HTML", StringCell.TYPE).createSpec();

		return new DataTableSpec(allColSpecs);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset()
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs)
			throws InvalidSettingsException
	{
		pushFlowVariableString("html-content", "");
		return new DataTableSpec[] { createTableSpec() };
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings)
	{
		settingColumnFilter.saveSettingsTo(settings);
		settingNumRows.saveSettingsTo(settings);
		settingsDoubleFormat.saveSettingsTo(settings);
		settingsFormatDouble.saveSettingsTo(settings);
		settingIncludeRowid.saveSettingsTo(settings);
		settingsAddBasicStyle.saveSettingsTo(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings)
			throws InvalidSettingsException
	{
		settingColumnFilter.loadSettingsFrom(settings);
		settingNumRows.loadSettingsFrom(settings);
		settingsDoubleFormat.loadSettingsFrom(settings);
		settingsFormatDouble.loadSettingsFrom(settings);
		settingIncludeRowid.loadSettingsFrom(settings);
		settingsAddBasicStyle.loadSettingsFrom(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		settingColumnFilter.validateSettings(settings);
		settingNumRows.validateSettings(settings);
		settingsDoubleFormat.validateSettings(settings);
		settingsFormatDouble.validateSettings(settings);
		settingIncludeRowid.validateSettings(settings);
		settingsAddBasicStyle.validateSettings(settings);
		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{
		// TODO: generated method stub
	}
	
    /** {@inheritDoc} */
    @Override
    public InputPortRole[] getInputPortRoles() {
        return new InputPortRole[] {InputPortRole.NONDISTRIBUTED_STREAMABLE};
    }

    /** {@inheritDoc} */
    @Override
    public OutputPortRole[] getOutputPortRoles() {
        return new OutputPortRole[] {OutputPortRole.NONDISTRIBUTED};
    }
    
    /**
     * Identify which columns should be used to generate the table.
     * 
     * @param selected
     * @param spec
     * @return
     */
    protected List<String> getIncludedColumns(SettingsModelFilterString selected, DataTableSpec spec)
    {
		List<String> included = null;
		if(settingColumnFilter.isKeepAllSelected()) {
			String[] names = spec.getColumnNames();
			
			included = new ArrayList<String>();
			
			for(String colName : names)
			{
				if(TableToHtml.colSpecCompatible(spec.getColumnSpec(colName))) {
					included.add(colName);
				}
			}
			
		} else {
			included = selected.getIncludeList();
		}
		
		return included;
    }

	/** {@inheritDoc} */
	@Override
	public StreamableOperator createStreamableOperator(final PartitionInfo partitionInfo,
			final PortObjectSpec[] inSpecs) throws InvalidSettingsException
	{
		return new StreamableOperator()
		{

			@Override
			public StreamableOperatorInternals saveInternals()
			{
				return null;
			}

			@Override
			public void runFinal(final PortInput[] inputs, final PortOutput[] outputs, final ExecutionContext ctx)
					throws Exception
			{
				
				List<String> included = getIncludedColumns(settingColumnFilter, ((RowInput)inputs[0]).getDataTableSpec());
				

				streamConverter = new TableToHtml(settingNumRows.getIntValue(), settingsDoubleFormat.getStringValue(), settingIncludeRowid.getBooleanValue(), settingsAddBasicStyle.getBooleanValue());
				
				streamTrack = 0;
				
				RowInput in = (RowInput) inputs[0];
				streamConverter.convertToHtmlStreamStart(in.getDataTableSpec(), included);
				
				
				RowOutput out = (RowOutput) outputs[0];
				TableToHtmlNodeModel.this.execute(in, ctx);
				
				String html = streamConverter.convertToHtmlStreamEnd();
				
				out.push(createRow(html));
				out.close();
				
				pushFlowVariableString("html-content", html);
			}
		};
	}
	
	private void execute(final RowInput inData, final ExecutionContext exec) throws Exception
	{
		
		DataRow row;
		while ((row = inData.poll()) != null && streamTrack < settingNumRows.getIntValue())
		{
			streamConverter.addRow(row);
			streamTrack++;
		}
		
		inData.close();
	}

}
