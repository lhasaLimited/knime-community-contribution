/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.htmltotable.util;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.knime.core.data.BooleanValue;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataValue;
import org.knime.core.data.DoubleValue;
import org.knime.core.data.IntValue;
import org.knime.core.data.LongValue;
import org.knime.core.data.StringValue;
import org.knime.core.data.def.BooleanCell;
import org.knime.core.node.BufferedDataTable;

/**
 * Converts a KNIME DataTable to a single HTML string. 
 * 
 * Approved types can be checked (use isCompatible) 
 * 
 * @author Samuel
 * @deprecated 09/01/2020 - see {@link TableToHtml2}
 */
public class TableToHtml 
{
	
	/**
	 * The approved column types
	 */
	@SuppressWarnings("unchecked")
	public static Class<? extends DataValue>[] APPROVED_TYPES = new Class[]{StringValue.class, IntValue.class, DoubleValue.class};
	
	/** the maximum number of rows to include */
	private int numRows;
	
	/** should the non default double formatting be used*/
	private boolean formatDouble;
	
	/** the configured decimal formatter */
	private DecimalFormat df; 
	
	/** should the row ID's be included*/
	private boolean includeRowID;
	
	private static final String CONSTANT_NEW_LINE = "\n";
	private static final String CONSTANT_TAB = "\t";
	private static final String CONSTANT_CELL_PADDING = "padding-top: 5px; padding-bottom: 5px; padding-left: 5px; padding-right: 5px";
	
	
	private StringBuffer buffer;
	List<Integer> includedIndexPosition;
	
	private boolean addBasicStyles;
	

	/**
	 * 
	 * @param numRows			the max number of output rows
	 * @param doubleFormat		the double format to sue
	 * @param includeRowID		whether to include the row ID
	 * @param addBasicStyles	whether to add basic styling
	 */
	public TableToHtml(int numRows,  String doubleFormat, boolean includeRowID, boolean addBasicStyles)
	{
		this.numRows = numRows;
		this.formatDouble = doubleFormat != null;
		this.df = new DecimalFormat();
		this.includeRowID = includeRowID;
		this.addBasicStyles = addBasicStyles;
		
		if(doubleFormat != null)
			this.df = new DecimalFormat(doubleFormat);
	}
	

	/**
	 * Start the output table: adds all the information we can before needing data
	 * 
	 * @param spec	the table spec
	 * @param list	the names of the columns to convert
	 */
	public void convertToHtmlStreamStart(DataTableSpec spec, List<String> list)
	{
		
		init(spec, list);

		
		// Start table
		buffer.append("<html><body><table border style=\"border:1px solid black\">");
		
		// start row
		buffer.append("\n \t <tr>");

		// Do the RowID first
		if(includeRowID)
		{	
			buffer.append("\t <td> <b> " + "RowID" + " </b> </td>");
		}
		
		
		for(Integer colIndex : includedIndexPosition)
		{
			buffer.append("\t <td> <b> " + spec.getColumnNames()[colIndex] + " </b> </td>");
		}
		

		buffer.append("\n \t </tr>");
		
	}
	
	/**
	 * Initialise the index positions and buffer
	 * @param spec
	 * @param list
	 */
	private void init(DataTableSpec spec, List<String> list)
	{
		includedIndexPosition = new ArrayList<Integer>();
		
		for(int i = 0; i < spec.getNumColumns(); i++)
			if(list.contains(spec.getColumnSpec(i).getName()))
				includedIndexPosition.add(i);
		
	
		buffer = new StringBuffer();
	}
	
	/**
	 * Get a series of tabs to give a specific level of indentation
	 * 
	 * @param num values >= 1
	 * @return
	 */
	private String getIndent(int num) 
	{		
		String indent = CONSTANT_TAB;
		
		for(int i = 1; i < num; i++)
			indent = indent + CONSTANT_TAB;
		
		return indent;
	}

	/**
	 * Add a new row to the HTML table
	 * @param row
	 */
	public void addRow(DataRow row)
	{
		buffer.append("\n \t <tr>");
		
		// Do the rowID first
		if(includeRowID)
		{
			buffer.append("\t <td> <b>" + row.getKey().toString() + "</b> </td>");
		}

		for(Integer colIndex : includedIndexPosition)
		{
			String cell;
			try
			{
				cell = "\t <td> " + getStringValue(row.getCell(colIndex)) + " </td>";
			} catch (Exception e)
			{
				cell = "\t <td> " + e.getMessage() + " </td>";
				e.printStackTrace();
			}

			buffer.append(cell);
		}
		
		// end row
		buffer.append("\t </tr>");
	}
	
	public String convertToHtmlStreamEnd()
	{
		// end table
		buffer.append("\n </table> </body></html>");
			
		return buffer.toString();
	}
	
	
	public String convertToHtml(BufferedDataTable table, List<String> list)
	{
		String html;
		
		if(addBasicStyles)
			html = convertToHtmlWithBasicStyles(table, list);
		else
			html = convertToHtmlNoStyles(table, list);
		
		return html;
	}
	
	/**
	 * Creates an HTML string representing the given table as an HTML table
	 * 
	 * @param table		The table to convert to HTML representation
	 * @param list		The columns to include
	 * 
	 * @return			The HTML string
	 */
	private String convertToHtmlNoStyles(BufferedDataTable table, List<String> list)
	{
		init(table.getSpec(), list);
		
		// Start table
		buffer.append("<html><body><table border style=\"border:1px solid black\">");
		
		// start row
		buffer.append("\n \t <tr>");

		// Do the RowID first
		if(includeRowID)
		{	
			buffer.append("\t <td> <b> " + "RowID" + " </b> </td>");
		}
		
		
		for(Integer colIndex : includedIndexPosition)
		{
			buffer.append("\t <td> <b> " + table.getSpec().getColumnNames()[colIndex] + " </b> </td>");
		}
		

		buffer.append("\n \t </tr>");

		// For each row, work through each cell creating a new cell	
		
		Iterator<DataRow> iterator = table.iterator();
		
		int track = 0;
		while(iterator.hasNext() && track < numRows)
		{
			DataRow row = iterator.next();
			
			addRow(row);
			
			track++;
		}
		
		// end table
		buffer.append("\n </table> </body></html>");
			
		return buffer.toString();
	}
		
	
	/**
	 * Creates an HTML string representing the given table as an HTML table. This method adds some basic styles to
	 * the HTML table: black header with white text, alternating white/grey row backgrounds.
	 * 
	 * @param table		The table to convert to HTML representation
	 * @param list		The columns to include
	 * 
	 * @return			The HTML string
	 */
	private String convertToHtmlWithBasicStyles(BufferedDataTable table, List<String> list)
	{
		
		// First identify the column index positions to include in the table
		DataTableSpec spec = table.getDataTableSpec();
		
		List<Integer> includedIndexPosition = new ArrayList<Integer>();
		
		for(int i = 0; i < spec.getNumColumns(); i++)
			if(list.contains(spec.getColumnSpec(i).getName()))
				includedIndexPosition.add(i);
		
	
		StringBuffer buffer = new StringBuffer();
		
		// Start table
		buffer.append("<html>");
		buffer.append(CONSTANT_NEW_LINE);
		buffer.append(getIndent(1) +  "<body>");
		buffer.append(CONSTANT_NEW_LINE);
		buffer.append(getIndent(2) + "<table border style=\"border:1px solid black; border-collapse: collapse\">");

		
		// Header row
		buffer.append(CONSTANT_NEW_LINE);
		buffer.append(getIndent(3) + "<tr style=\"background-color: black; color: white; " + CONSTANT_CELL_PADDING + "\">");

		buffer.append(CONSTANT_NEW_LINE);
		// Do the RowID first
		if(includeRowID)
			buffer.append(getIndent(4) + "<th> <b> " + "RowID" + " </b> </th>");

		
		for(Integer colIndex : includedIndexPosition)
		{
			buffer.append(CONSTANT_NEW_LINE);
			buffer.append(getIndent(4) + "<th> <b> " + spec.getColumnNames()[colIndex] + " </b> </th>");
		}

		buffer.append(CONSTANT_NEW_LINE);
		buffer.append(getIndent(3) + "</tr>");
		// End header row

		// For each row, work through each cell creating a new cell	
		
		Iterator<DataRow> iterator = table.iterator();
		
		int track = 0;
		while(iterator.hasNext() && track < numRows)
		{
			DataRow row = iterator.next();
			
			buffer.append(CONSTANT_NEW_LINE);
			buffer.append(getIndent(3) + "<tr style=\"padding: 0px;\">");
			
			// Do the rowID first
			if(includeRowID)
			{
				buffer.append(CONSTANT_NEW_LINE);
				buffer.append(getIndent(4) + createRowIdCell(row.getKey().toString(), track % 2 == 0));
			}

			for(Integer colIndex : includedIndexPosition)
			{
				String cell;
				try
				{
					if(row.getCell(colIndex).isMissing())
					{
						cell = getIndent(4) + createCell(getStringValue(row.getCell(colIndex)), track % 2 == 0, "color: red;");
					}
					else
					{
						cell = getIndent(4) + createCell(getStringValue(row.getCell(colIndex)), track % 2 == 0);
					}
					

				} catch (Exception e)
				{
					cell = getIndent(4) + createCell(e.getMessage(), track % 2 == 0, "color: red;");
					e.printStackTrace();
				}
				buffer.append(CONSTANT_NEW_LINE);
				buffer.append(cell);
			}
			
			// end row
			buffer.append(CONSTANT_NEW_LINE);
			buffer.append(getIndent(3) + "</tr>");
			
			track++;
		}
		
		// end table
		buffer.append(CONSTANT_NEW_LINE);
		buffer.append(getIndent(2) + "</table>");
		buffer.append(CONSTANT_NEW_LINE);
		buffer.append(getIndent(1) + "</body>");
		buffer.append(CONSTANT_NEW_LINE);
		buffer.append("</html>");
		
		return buffer.toString();
	}
	
	/**
	 * Create a new HTML table cell
	 * 
	 * @param value						the value of the cell
	 * @param setBackgroundColour		whether to set the background colour
	 * @param styles					any additional styles to set (; seperated string)
	 * @return
	 */
	private String createCell(String value, boolean setBackgroundColour, String styles) 
	{
		String style = "border: 1px solid #ddd;";
		
		if(setBackgroundColour)
			style = style + "background-color: #f2f2f2;";
		
		if(styles != null)
			style = style + styles;
		
		return "<td style=\""+ style + "\"> <b>" + value + "</b> </td>";
	}

	public String createRowIdCell(String value, boolean b)
	{
		return createCell(value, b, null);
	}
	
	public String createCell(String value, boolean b)
	{
		String style = "border: 1px solid #ddd;";
		
		if(b)
			style = style + "background-color: #f2f2f2;";
		
		
		
		return "<td style=\""+ style + "\">" + value + "</td>";
	}
	
	
	
	/**
	 * Get the string representation of a given cell
	 * 
	 * @param cell				The cell to get the string representation of
	 * 
	 * @return					A string value representing the cells contents
	 * 
	 * @throws Exception		An exception can be thrown if the given cell is not an approved type or if the cell type
	 * 							has not been implemented yet. There is a cascading series of if else statements to get
	 * 							the string representation. 
	 */
	private String getStringValue(DataCell cell) throws Exception
	{
		String formatted = "";
		
		if(!isApprovedType(cell))
		{
			throw new Exception("Invalid cell type (" + cell.getClass() + ") - allowed types: " + Arrays.toString(APPROVED_TYPES));
		}
		
		
		if(cell.isMissing())
		{
			formatted = "?";
		}
		else if(cell.getType().isCompatible((StringValue.class)))
		{
			formatted = ((StringValue) cell).getStringValue();
			
		} else if(cell.getType().isCompatible(BooleanValue.class))
		{
			formatted = ((BooleanCell)cell).toString();
		}
		else if(cell.getType().isCompatible(LongValue.class))
		{
			formatted = Long.toString(((LongValue)cell).getLongValue());
		}
		else if(cell.getType().isCompatible(IntValue.class))
		{
			formatted = Integer.toString(((IntValue) cell).getIntValue());
		} else if(cell.getType().isCompatible(DoubleValue.class))
		{
			double value = ((DoubleValue) cell).getDoubleValue();
					
			if(Double.isNaN(value) || Double.isInfinite(value))
			{
				formatted = Double.toString(value);
			}
			else
			{
				formatted = formatDouble ? formatted = df.format(value) : Double.toString(value);
			}

		} else
		{
			throw new Exception("Cell type (" + cell.getClass() + ") not yet implemented - allowed types: " + Arrays.toString(APPROVED_TYPES));
		}
			
		return formatted;
	}
	
	
	
	/**
	 * Checks whether the given cell is an approved column type. The approved types
	 * are detailed in the APPROVED_TYPES array. 
	 * 
	 * Checks isCompatible with. 
	 * 
	 * @param cell		The cell to check the type of
	 * 
	 * @return
	 */
	private boolean isApprovedType(DataCell cell)
	{
		boolean approved = false;
		
		for(int i = 0; i< APPROVED_TYPES.length && approved == false; i++)
		{
			approved = cell.getType().isCompatible(APPROVED_TYPES[i]);
		}

		return approved;
	}

	/**
	 * Identifies if the given column specification is compatible
	 * @param columnSpec
	 * @return
	 */
	public static boolean colSpecCompatible(DataColumnSpec columnSpec) 
	{
		boolean compatible = false;
	
		for(int i = 0; i < APPROVED_TYPES.length && !compatible; i++) 
		{
			compatible = columnSpec.getType().isCompatible(APPROVED_TYPES[i]);
		}
		
		return compatible;
	}
	
	
	
	
}