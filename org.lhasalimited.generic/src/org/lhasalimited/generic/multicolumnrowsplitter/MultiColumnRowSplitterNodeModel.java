/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.multicolumnrowsplitter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.knime.base.node.preproc.filter.row.RowFilterNodeModel;
import org.knime.base.node.preproc.filter.row.rowfilter.ColValFilterOldObsolete;
import org.knime.base.node.preproc.filter.row.rowfilter.EndOfTableException;
import org.knime.base.node.preproc.filter.row.rowfilter.IRowFilter;
import org.knime.base.node.preproc.filter.row.rowfilter.IncludeFromNowOn;
import org.knime.base.node.preproc.filter.row.rowfilter.MissingValueRowFilter;
import org.knime.base.node.preproc.filter.row.rowfilter.RangeRowFilter;
import org.knime.base.node.preproc.filter.row.rowfilter.RowFilterFactory;
import org.knime.base.node.preproc.filter.row.rowfilter.StringCompareRowFilter;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTable;
import org.knime.core.data.DataTableSpec;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.port.PortObjectSpec;
import org.knime.core.node.streamable.InputPortRole;
import org.knime.core.node.streamable.OutputPortRole;
import org.knime.core.node.streamable.PartitionInfo;
import org.knime.core.node.streamable.PortInput;
import org.knime.core.node.streamable.PortOutput;
import org.knime.core.node.streamable.RowInput;
import org.knime.core.node.streamable.RowOutput;
import org.knime.core.node.streamable.StreamableOperator;
import org.knime.core.node.streamable.StreamableOperatorInternals;

/**
 * This is the model implementation of MultiColumnRowSplitter. Row splitter
 * functionality matching across multiple columns. <br>
 * <br>
 *
 * Heavily based on {@link RowFilterNodeModel} and adjusted to work on multiple
 * columns and therefore is GPL v3 <br>
 * <br>
 * A couple of approaches were taken. Firstly creating a RowFilter per column
 * and saving these. However, linking to flow variables became troubleseome. The
 * current approach is to save a single RowFilter and then duplicate then in the
 * {@link RowRowFilterNodeModel#execute} and
 * {@link RowRowFilterNodeModel#configure} methods. However, there is a work
 * around required to work with the RangeFilter. The RangeFilter for the first
 * column in this list is saved; the comparitor for the RowFilter different
 * depending on the type of column. The RangeRowFilter has been extended to
 * {@link RangeRowFilterExtended} and a constructor is available that takes the
 * saved RangeRowFilter and will build itself and auto calculate the comparitor
 * type.
 *
 * @since 2.11
 * @author Lhasa Limited (Samuel)
 */
public class MultiColumnRowSplitterNodeModel extends NodeModel
{

	private static NodeLogger LOGGER;

	// Configuration keys
	/** the key for the selected columns */
	public static final String CONFIG_SELECTED_COLUMNS = "SELECTED_COLUMNS";

	/** key for storing settings in config object. */
	static final String CFGFILTER = "rowFilter";

	/** the config key for matching all */
	public static final String CONFIG_MATCH_ALL = "SINGLE_MATCH";

	/** the config key for the number of filters */
	public static final String CONFIG_NUM_FILTERS = "NUM_FILTERS";

	/** the config key for exclude match */
	public static final String CONFIG_EXCLUDE = "EXCLUDE";

	// Variables
	/** the columns selected for filtering on */
	private String[] m_columns;

	/**
	 * the row filter, carry over from RowFilterNodeModel however to allow
	 * linking with the flow variables we only use the one and then duplicate it
	 */
	private IRowFilter m_rowFilter;

	/** the row filters */
	private List<IRowFilter> m_rowFilters;

	/** match all filters */
	private boolean m_matchAll;

	/** is the match configured to perform an exclusion */
	private boolean m_exclude;

	/**
	 * Constructor for the node model.
	 */
	protected MultiColumnRowSplitterNodeModel()
	{
		super(1, 2); // We are doing a split not a filter so need 2 outputs

		m_rowFilter = null;
		m_rowFilters = null;

		LOGGER = NodeLogger.getLogger(getClass());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec)
			throws Exception
	{
		// Get the input data and configure the output data containers
		DataTable in = inData[0];
		BufferedDataContainer containerInclude = exec.createDataContainer(in.getDataTableSpec());
		BufferedDataContainer containerExclude = exec.createDataContainer(in.getDataTableSpec());

		// in case the node was configured and the workflow is closed
		// (and saved), the row filter isn't configured upon reloading.
		// here, we give it a chance to configure itself (e.g. find the column
		// index)
		m_rowFilter.configure(in.getDataTableSpec());

		// Now we need to create all the RowFilters and do the configuration
		// like above
		// m_rowFilters = new ArrayList<RowFilter>();
		m_rowFilters = createFilters(in.getDataTableSpec(), m_rowFilter, m_columns);

		LOGGER.info("Filters configured: " + m_rowFilters.toString());

		if (m_rowFilter instanceof RangeRowFilter && checkMixedTypes(m_rowFilters))
		{
			LOGGER.warn(
					"Multiple column types have been selected under range based filter, may produce unexpected behaviour");
			setWarningMessage(
					"Multiple column types have been selected under range based filter, may produce unexpected behaviour");
		}

		exec.setMessage("Searching first matching row...");

		// Iterate through each row and identify if a match
		Iterator<DataRow> rowIterator = in.iterator();
		int track = 0;
		while (rowIterator.hasNext())
		{
			exec.checkCanceled();
			DataRow row = rowIterator.next();

			boolean approved = checkForApproval(row, m_rowFilters, m_matchAll, m_exclude, track);

			if (approved)
				containerInclude.addRowToTable(row);
			else
				containerExclude.addRowToTable(row);

			exec.setProgress((double) track / inData[0].size(), "Filtering rows");
			track++;
		}

		containerExclude.close();
		containerInclude.close();

		return new BufferedDataTable[] { containerInclude.getTable(), containerExclude.getTable() };
	}

	/**
	 * Take the input specification, the filter that has been saved and generate
	 * the appropriate filters for the given column spec.
	 * 
	 * @param in
	 *            The input DataTableSpec
	 * @param mainFilter
	 *            The main filter (the one we save)
	 * @param selectedColumns
	 *            The columns selected
	 * @return
	 * @throws Exception
	 *             Will throw if given a ListCell type (so does the
	 *             {@link RowFilterNodeModel}
	 */
	private List<IRowFilter> createFilters(DataTableSpec in, IRowFilter mainFilter, String[] selectedColumns)
			throws Exception
	{
		List<IRowFilter> filters = new ArrayList<>();

		if (mainFilter instanceof StringCompareRowFilter)
		{
			StringCompareRowFilter filter = ((StringCompareRowFilter) mainFilter);

			for (int i = 0; i < selectedColumns.length; i++)
				filters.add(new StringCompareRowFilter(filter.getPattern(), selectedColumns[i], filter.getInclude(),
						filter.getCaseSensitive(), filter.getHasWildcards(), filter.getIsRegExpr()));

		} else if (mainFilter instanceof RangeRowFilter)
		{
			RangeRowFilter filter = ((RangeRowFilter) mainFilter);

			for (int i = 0; i < selectedColumns.length; i++)
			{
				filters.add(
						new RangeRowFilterExtended(filter, in.getColumnSpec(in.findColumnIndex(selectedColumns[i]))));
			}

		} else if (mainFilter instanceof MissingValueRowFilter)
		{
			MissingValueRowFilter filter = ((MissingValueRowFilter) mainFilter);

			for (int i = 0; i < selectedColumns.length; i++)
				filters.add(
						new MissingValueRowFilter(selectedColumns[i], filter.getInclude(), filter.getDeepFiltering()));

		} else if (mainFilter instanceof ColValFilterOldObsolete)
		{
			// This was deprecated pre-reusing the existing classes. Not
			// expected that this is ever created
			// so we don't bother trying to kludge this in
			throw new Exception("Not implemented as this was not expected to be ever used");
		}

		// We need to do the above for all our filters
		// can we do this as we add it to the List?
		for (int i = 0; i < filters.size(); i++)
			filters.get(i).configure(in);

		return filters;
	}

	/**
	 * Checks whether the given rows matches the filters. Provide information on
	 * whether it must match all filters or only a single. Behaviour differs if
	 * doing an exclusion match. <br>
	 * <br>
	 * Currently counts the number of matched and unmatched filters to allow for
	 * future extension such as % of columns matched > value. However this could
	 * potentially be sped up if we terminate matching as soon as the answer is
	 * known under the current all or nothing approach.
	 * 
	 * 
	 * @param row
	 *            The data row to check for approval
	 * @param filters
	 *            The filters to apply to the given data row
	 * @param matchAll
	 *            Should all filters have to match?
	 * @param exclude
	 *            Is this an exclusion or an inclusion filter?
	 * @param rowIndex
	 *            The row index
	 * @return true if the row matches the given setup, false if not
	 * 
	 * @throws EndOfTableException
	 * @throws IncludeFromNowOn
	 */
	private boolean checkForApproval(DataRow row, List<IRowFilter> filters, boolean matchAll, boolean exclude,
			int rowIndex) throws EndOfTableException, IncludeFromNowOn
	{

		int matchCount = 0;
		int unmatchCount = 0;

		for (int i = 0; i < filters.size(); i++)
		{
			if (filters.get(i).matches(row, rowIndex))
				matchCount++;
			else
				unmatchCount++;
		}

		boolean match;

		if (exclude)
		{
			// For an exclusion match a column matches if the matcher doesn't
			// match
			if (matchAll)
			{
				// At least 1 filter must return a match (i.e. didn't find the
				// excluded filter)
				match = matchCount != 0 ? true : false;
			} else
			{
				// all columns must match (therefore none hitting the excluded
				// filter)
				match = unmatchCount == 0 ? true : false;
			}
		} else
		{
			// no columns can be unmatched only 1 column needs to match
			match = matchAll ? (unmatchCount > 0 ? false : true) : (matchCount > 0 ? true : false);
		}

		return match;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset()
	{

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException
	{

		if (m_rowFilter == null)
			throw new InvalidSettingsException("No row filter specified");

		List<IRowFilter> filters = null;

		try
		{
			filters = createFilters(inSpecs[0], m_rowFilter, m_columns);
		} catch (Exception e)
		{
			e.printStackTrace();
			LOGGER.error("Failed to configure the filters", e);
			throw new InvalidSettingsException(e);
		}

		if (m_rowFilter instanceof RangeRowFilter && checkMixedTypes(filters))
		{
			LOGGER.warn(
					"Multiple column types have been selected under range based filter, may produce unexpected behaviour");
			setWarningMessage(
					"Multiple column types have been selected under range based filter, may produce unexpected behaviour");
		}

		return new DataTableSpec[] { inSpecs[0], inSpecs[0] };
	}

	/**
	 * Checks to see if the given list of filters contains mutliple cell types
	 * Will return false if the filter option isn't RangeRowFilter
	 * 
	 * @param filters
	 * 
	 * @return
	 */
	private boolean checkMixedTypes(List<IRowFilter> filters)
	{
		boolean mixed = false;

		if (m_rowFilter instanceof RangeRowFilter)
		{
			Set<String> types = new TreeSet<>();

			for (IRowFilter filter : filters)
				types.add(((RangeRowFilter) filter).getLowerBound().getType().toString());

			if (types.size() > 1)
				mixed = true;
		}

		return mixed;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings)
	{
		assert settings != null;

		if (m_rowFilter != null)
		{
			System.out.println("Saving row filter (main): " + m_rowFilter);
			NodeSettingsWO filterCfg = settings.addNodeSettings(CFGFILTER);
			m_rowFilter.saveSettingsTo(filterCfg);
		}

		settings.addBoolean(MultiColumnRowSplitterNodeModel.CONFIG_EXCLUDE, m_exclude);
		settings.addBoolean(MultiColumnRowSplitterNodeModel.CONFIG_MATCH_ALL, m_matchAll);
		settings.addStringArray(MultiColumnRowSplitterNodeModel.CONFIG_SELECTED_COLUMNS, m_columns);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		loadOrValidateSettingsFrom(settings, false);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		loadOrValidateSettingsFrom(settings, true);
	}

	/**
	 * Taken from {@link RowFilterNodeModel}
	 * 
	 * @param settings
	 * @param verifyOnly
	 * @throws InvalidSettingsException
	 */
	private void loadOrValidateSettingsFrom(final NodeSettingsRO settings, final boolean verifyOnly)
			throws InvalidSettingsException
	{
		IRowFilter tmpFilter = null;

		// int numFilters = settings.getInt(CONFIG_NUM_FILTERS);
		//
		// List<RowFilter> tempFilters = new ArrayList<RowFilter>();

		if (settings.containsKey(CFGFILTER))
		{
			NodeSettingsRO filterCfg = settings.getNodeSettings(CFGFILTER);
			// because we don't know what type of filter is in the config we
			// must ask the factory to figure it out for us (actually the type
			// is also saved in a valid config). When we save row filters they
			// will (hopefully!!) add their type to the config.
			tmpFilter = RowFilterFactory.createRowFilter(filterCfg);

		} else
		{
			throw new InvalidSettingsException("Row Filter config contains no row filter.");
		}

		// if we got so far settings are valid.
		if (verifyOnly)
		{
			return;
		}

		// take over settings
		m_rowFilter = tmpFilter;
		// m_rowFilters = tempFilters;
		m_matchAll = settings.getBoolean(CONFIG_MATCH_ALL);
		m_exclude = settings.getBoolean(CONFIG_EXCLUDE);

		m_columns = settings.getStringArray(CONFIG_SELECTED_COLUMNS);

		return;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{
		// TODO: generated method stub
	}
	
    /** {@inheritDoc} */
    @Override
    public InputPortRole[] getInputPortRoles() {
        return new InputPortRole[] {InputPortRole.NONDISTRIBUTED_STREAMABLE};
    }

    /** {@inheritDoc} */
    @Override
    public OutputPortRole[] getOutputPortRoles() {
        return new OutputPortRole[] {OutputPortRole.NONDISTRIBUTED};
    }

	/** {@inheritDoc} */
	@Override
	public StreamableOperator createStreamableOperator(final PartitionInfo partitionInfo,
			final PortObjectSpec[] inSpecs) throws InvalidSettingsException
	{
		return new StreamableOperator()
		{

			@Override
			public StreamableOperatorInternals saveInternals()
			{
				return null;
			}

			@Override
			public void runFinal(final PortInput[] inputs, final PortOutput[] outputs, final ExecutionContext ctx)
					throws Exception
			{
				RowInput in = (RowInput) inputs[0];
				RowOutput out = (RowOutput) outputs[0];
				RowOutput out2 = (RowOutput) outputs[1];
				MultiColumnRowSplitterNodeModel.this.execute(in, new RowOutput[]{out, out2}, ctx);
			}
		};
	}

	private void execute(final RowInput inData, final RowOutput[] output, final ExecutionContext exec) throws Exception
	{
		// in case the node was configured and the workflow is closed
		// (and saved), the row filter isn't configured upon reloading.
		// here, we give it a chance to configure itself (e.g. find the column
		// index)
		m_rowFilter.configure(inData.getDataTableSpec());

		// Now we need to create all the RowFilters and do the configuration
		// like above
		// m_rowFilters = new ArrayList<RowFilter>();
		m_rowFilters = createFilters(inData.getDataTableSpec(), m_rowFilter, m_columns);

		LOGGER.info("Filters configured: " + m_rowFilters.toString());

		if (m_rowFilter instanceof RangeRowFilter && checkMixedTypes(m_rowFilters))
		{
			LOGGER.warn(
					"Multiple column types have been selected under range based filter, may produce unexpected behaviour");
			setWarningMessage(
					"Multiple column types have been selected under range based filter, may produce unexpected behaviour");
		}

		exec.setMessage("Searching first matching row...");
		DataRow row;
		int index = 0;
		boolean isAlwaysExcluded = false;
		boolean isAlwaysIncluded = false;
		while ((row = inData.poll()) != null)
		{
			boolean matches;
			if (isAlwaysExcluded)
			{
				matches = false;
			} else if (isAlwaysIncluded)
			{
				matches = true;
			} else
			{
				try
				{
					matches = checkForApproval(row, m_rowFilters, m_matchAll, m_exclude, index++);
				} catch (EndOfTableException eot)
				{
					break;
				} catch (IncludeFromNowOn ifn)
				{
					isAlwaysIncluded = true;
					matches = true;
				}
			}
			exec.checkCanceled();
			if (matches)
			{
				exec.setMessage("Added row " + index + " (\"" + row.getKey() + "\")");
				output[0].push(row);
			} else
			{
				output[1].push(row);
			}
		}
		inData.close();
		output[0].close();
		output[1].close();
	}

}
