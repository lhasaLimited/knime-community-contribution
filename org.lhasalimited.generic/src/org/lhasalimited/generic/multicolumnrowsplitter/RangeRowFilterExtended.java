/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.multicolumnrowsplitter;

import org.knime.base.node.preproc.filter.row.rowfilter.RangeRowFilter;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataType;
import org.knime.core.data.DoubleValue;
import org.knime.core.data.IntValue;
import org.knime.core.data.LongValue;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.LongCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.InvalidSettingsException;

/**
 * Extended {@link RangeRowFilter} to allow for the reconfigure given a column column {@link DataType}. 
 * For use with the MultiColumnRowFilter which applies the same filter to multiple columns. 
 * 
 * Contains code adapted from {@link ColumnFilterRowPanel}
 * 
 * @author Samuel
 *
 */
public class RangeRowFilterExtended extends RangeRowFilter
{

	/**
	 * Simply calls the super constructor, was required to extend the {@link RangeRowFilter}, not intended to be
	 * used as {@link RangeRowFilterExtended#RangeRowFilterExtended(RangeRowFilter, DataColumnSpec)} handled the
	 * reconfiguring to the given column spec for you. 
	 * 
	 * @param colName			the column name
	 * @param include			if true, matching rows are included, if false, they are excluded.
	 * @param lowerBound		the lower bound of the range the value will be checked against. Could be null (if the upper bound is not null) which indicates that there is no lower bound for the range.
	 * @param upperBound		the upper bound of the range the value will be checked against. Could be null (if the lower bound is not null) which indicates that there is no upper bound for the range.
	 */
	public RangeRowFilterExtended(String colName, boolean include, DataCell lowerBound, DataCell upperBound)
	{
		super(colName, include, lowerBound, upperBound);
	}
	
	/**
	 * Provide the original {@link RangeRowFilter} and create a new {@link RangeRowFilterExtended} to support
	 * the give column specification. 
	 * <br><br>
	 * For example if a RangeRowFilter for a String column is provided (using the StringComparator) and the 
	 * column spec provided is a Double column then then the lower and upper bounds will be parsed to
	 * DoubleCells for creating the new RangeRowFilterExtended. This will then result in the generation
	 * of a DoubleComparator when the filter is configured. 
	 * 
	 * 
	 * @param filter
	 * @param colSpec
	 * @throws InvalidSettingsException
	 */
	public RangeRowFilterExtended(RangeRowFilter filter, DataColumnSpec colSpec) throws InvalidSettingsException
	{
		super(colSpec.getName(), filter.getInclude(), getCell(colSpec.getName(), colSpec.getType(), filter.getLowerBound()), getCell(colSpec.getName(), colSpec.getType(), filter.getUpperBound()));
	}

	
	
	/**
	 * Creates a new boundCell (just a {@link DataCell}) for the given column {@link DataType}. For example if the
	 * column {@link DataType} is a {@link StringCell} and the boundCell is a {@link DoubleCell} then a new {@link StringCell}
	 * will be created containing the value of the {@link DoubleCell}. 
	 * 
	 * @param name								The column name
	 * @param cType								The {@link DataType} of the column
	 * @param boundCell							The bounded cell containing the value
	 * 
	 * @return									A new {@link DataCell} suitable for the given column type
	 * @throws InvalidSettingsException
	 */
	private static DataCell getCell(String name, DataType cType, DataCell boundCell) throws InvalidSettingsException
	{
		
		if(boundCell == null)
			return null;
		
		String stingVal = boundCell.toString();
		
		if (cType.isCompatible(IntValue.class))
		{
			// first try making of an IntCell
			try
			{
				int lb;
				if(boundCell.getType().isCompatible(IntValue.class))
				{
					lb = ((IntValue)boundCell).getIntValue();
				} else if(boundCell.getType().isCompatible(DoubleValue.class))
				{
					lb = (int)((DoubleValue)boundCell).getDoubleValue();
				}
				else
				{
					lb = Integer.parseInt(stingVal);
				}

				return new IntCell(lb);
			} catch (NumberFormatException nfe)
			{
				throw new InvalidSettingsException("Number format error in " + name
						+ " bound number: Enter a valid integer.");
			}
		} else if (cType.isCompatible(LongValue.class))
		{
			try
			{
				long lb = Long.parseLong(stingVal);
				return new LongCell(lb);
			} catch (NumberFormatException nfe)
			{
				throw new InvalidSettingsException("Number format error in " + name
						+ " bound number: Enter a valid number.");
			}

		} else if (cType.isCompatible(DoubleValue.class))
		{
			try
			{
				double lb = Double.parseDouble(stingVal);
				return new DoubleCell(lb);
			} catch (NumberFormatException nfe)
			{
				throw new InvalidSettingsException("Number format error in " + name
						+ " bound number: enter a valid " + "float number");
			}
		} else
		{
			return new StringCell(stingVal);
		}

	}

}
