package org.lhasalimited.generic.io.writer.strings;

import org.knime.core.node.NodeDialogPane;
import org.knime.core.node.NodeFactory;
import org.knime.core.node.NodeView;

/**
 * <code>NodeFactory</code> for the "StringCellToFile" Node.
 * Writes the contents of a StringValue to a file
 *
 * @author Samuel, Lhasa Limited
 */
public class StringCellToFileNodeFactory 
        extends NodeFactory<StringCellToFileNodeModel> {

    /**
     * {@inheritDoc}
     */
    @Override
    public StringCellToFileNodeModel createNodeModel() {
        return new StringCellToFileNodeModel();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNrNodeViews() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeView<StringCellToFileNodeModel> createNodeView(final int viewIndex,
            final StringCellToFileNodeModel nodeModel) {
        return new StringCellToFileNodeView(nodeModel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasDialog() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NodeDialogPane createNodeDialogPane() {
        return new StringCellToFileNodeDialog();
    }

}

