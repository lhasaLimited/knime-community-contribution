package org.lhasalimited.generic.io.writer.strings;

import java.io.File;
import java.io.FileWriter;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.StringValue;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.container.SingleCellFactory;
import org.knime.core.data.def.BooleanCell.BooleanCellFactory;
import org.knime.core.data.def.StringCell.StringCellFactory;
import org.knime.core.node.InvalidSettingsException;
import org.lhasalimited.generic.node.config.StreamableLocalSettingsNodeModel;

/**
 * This is the model implementation of StringCellToFile. Writes the contents of
 * a StringValue to a file
 *
 * @author Samuel, Lhasa Limited
 */
public class StringCellToFileNodeModel extends StreamableLocalSettingsNodeModel<StringCellToFileSettings>
{

	@Override
	protected ColumnRearranger createColumnRearrangerImplementation(final DataTableSpec spec) throws InvalidSettingsException
	{
		DataColumnSpec newSpec = new DataColumnSpecCreator(DataTableSpec.getUniqueColumnName(spec, "FileWritten"),
				BooleanCellFactory.TYPE).createSpec();

		ColumnRearranger rearranger = new ColumnRearranger(spec);
		rearranger.append(new SingleCellFactory(true, newSpec)
		{

			@Override
			public DataCell getCell(DataRow row)
			{
				
				DataCell valueCell = row.getCell(spec.findColumnIndex(localSettings.getStringColumnName()));
				
				DataCell filenameCell;
				if(localSettings.useRowID())
				{
					filenameCell = StringCellFactory.create(row.getKey().getString());
				} 
				else
				{
					filenameCell = row.getCell(spec.findColumnIndex(localSettings.getFilenameColumnName()));
				}
				 
				DataCell outcell;
				if(valueCell.isMissing() || filenameCell.isMissing())
				{
					outcell = BooleanCellFactory.create(false);
				} else
				{
					
					String value = ((StringValue) valueCell).getStringValue();
					String filename = ((StringValue) filenameCell).getStringValue();
					
					File file = localSettings.getFile(filename);
					
					if(file.exists() && !localSettings.getIsOveride())
					{
						outcell = BooleanCellFactory.create(false);
						getLogger().error("File already exists: " + row.getKey());
						setWarningMessage("File(s) already exist and have not been replaced");
					} else
					{
						try
						{
							if(file.exists())
								if(!file.delete())
									throw new Exception("File not deleted");
							
							FileWriter fileWriter = new FileWriter(file);
							
							fileWriter.write(value);
							
							fileWriter.close();
						} catch (Exception e)
						{
							outcell = BooleanCellFactory.create(false);
							getLogger().error("Error writing file: " + row.getKey(), e);
						}
						
						outcell = BooleanCellFactory.create(true);
					}
				}
				
				return outcell;
			}
		});

		return rearranger;
	}

	@Override
	protected DataTableSpec[] configure(DataTableSpec[] inSpecs) throws InvalidSettingsException {
		
		if(!localSettings.getDirectoryFile().exists())
			throw new InvalidSettingsException("The chosen directory " + localSettings.getDirectoryFile() + " does not exist");

		return super.configure(inSpecs);
	}

}
