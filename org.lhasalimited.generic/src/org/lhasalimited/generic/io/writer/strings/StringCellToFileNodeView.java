package org.lhasalimited.generic.io.writer.strings;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "StringCellToFile" Node.
 * Writes the contents of a StringValue to a file
 *
 * @author Samuel, Lhasa Limited
 */
public class StringCellToFileNodeView extends NodeView<StringCellToFileNodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link StringCellToFileNodeModel})
     */
    protected StringCellToFileNodeView(final StringCellToFileNodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

