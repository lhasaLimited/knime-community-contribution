/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.multiperformance3;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataTableSpecCreator;
import org.knime.core.data.RowIterator;
import org.knime.core.data.RowKey;
import org.knime.core.data.append.AppendedColumnRow;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.DoubleCell.DoubleCellFactory;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.IntCell.IntCellFactory;
import org.knime.core.data.def.StringCell;
import org.knime.core.data.def.StringCell.StringCellFactory;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.port.PortObjectSpec;
import org.knime.core.node.streamable.InputPortRole;
import org.knime.core.node.streamable.OutputPortRole;
import org.knime.core.node.streamable.PartitionInfo;
import org.knime.core.node.streamable.PortInput;
import org.knime.core.node.streamable.PortOutput;
import org.knime.core.node.streamable.RowInput;
import org.knime.core.node.streamable.RowOutput;
import org.knime.core.node.streamable.StreamableOperator;
import org.knime.core.node.streamable.StreamableOperatorInternals;
import org.lhasalimited.generic.util.Result;
import org.lhasalimited.generic.util.Results;

/**
 * This is the model implementation of MultiPerformance. Calculate the
 * performance of multiple models which are provided as seperate columns
 *
 * @author Lhasa Limited
 */
public class MultiPerformanceNodeModel3 extends NodeModel
{

	/* start of variables for include exclude */
	/** string contains "selected properties" */
	static final String CONFIG_PREDICTION_COLUMNS = "selected properties";

	/** identifies the included and excluded column selections */
	private final SettingsModelFilterString settingSelectedPredictionColumns = new SettingsModelFilterString(
			CONFIG_PREDICTION_COLUMNS, new ArrayList<String>(), new ArrayList<String>());
	/* end of variables for include exclude */

	// Activity column
	static final String CONFIG_ACTIVITY_COLUMN = "Activity column";
	private final SettingsModelColumnName settingActivityColumn = new SettingsModelColumnName(CONFIG_ACTIVITY_COLUMN, "");

	// String entries
	static final String CONFIG_ACTIVE_STRING = "Active flag";
	private final SettingsModelString settingActiveString = new SettingsModelString(CONFIG_ACTIVE_STRING, "active");
	
	static final String CONFIG_INACTIVE_STRING = "Inactive flag";
	private final SettingsModelString settingInactiveString = new SettingsModelString(CONFIG_INACTIVE_STRING,
			"inactive");
	
	static final String CONFIG_EQUIVOCAL_STRING = "Equivocal flag";
	private final SettingsModelString settingEquivocalString = new SettingsModelString(CONFIG_EQUIVOCAL_STRING,
			"equivocal");
	
	
	static final String CONFIG_OUT_OF_DOMAIN_STRING = "Out of domain flag";
	private final SettingsModelString settingOutOfDomainString = new SettingsModelString(CONFIG_OUT_OF_DOMAIN_STRING,
			"out of domain");
	
	static final String CONFIG_MISSING_VALUE = "Missing as out of domain";
	private final SettingsModelBoolean settingMissingValueAsOut = new SettingsModelBoolean(CONFIG_MISSING_VALUE, true);

	private static NodeLogger LOGGER = NodeLogger.getLogger(MultiPerformanceNodeModel3.class);

	private final int numOutCols = 22;

	private List<Results> results;

	/**
	 * Constructor for the node model.
	 */
	protected MultiPerformanceNodeModel3()
	{
		super(1, 2); // Outputs: {The predictions, the rows with issues}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec)
			throws Exception
	{

		/** data from input port 0 */
		BufferedDataTable input = inData[0];
		/** specification of input data table */
		DataTableSpec inDataSpec = inData[0].getDataTableSpec();

		// The output container for the rows with issues
		// As implemented any issue on any column will put
		// the row to this container. However, that does not
		// mean it didn't successfully get added to a specific
		// set of results.
		BufferedDataContainer containerIssues = exec.createDataContainer(createErrorSpec(inDataSpec));

		// Configuration
		/** string list of includedColumns containing the prediction values */
		List<String> includedColumns = settingSelectedPredictionColumns.getIncludeList();

		// Get these now to make life easier
		String activityColumnName = settingActivityColumn.getColumnName();
		int activityColumnIndex = inDataSpec.findColumnIndex(activityColumnName);

		/** the index positions of the prediction columns selected */
		int[] predictionColumnPositions = new int[includedColumns.size()];
		for (int i = 0; i < predictionColumnPositions.length; i++)
		{
			predictionColumnPositions[i] = inDataSpec.findColumnIndex(includedColumns.get(i));
		}

		// Create the map of the predicted values to the enum values
		// This can then be passed into the constructor for the results
		// that need to be created per column
		/** the array of results for the columns selected */
		List<Results> results = new ArrayList<Results>();
		Map<Result, String> stringMap = new TreeMap<Result, String>();
		stringMap.put(Result.EQUIVOCAL, settingEquivocalString.getStringValue());
		stringMap.put(Result.ACTIVE, settingActiveString.getStringValue());
		stringMap.put(Result.INACTIVE, settingInactiveString.getStringValue());
		stringMap.put(Result.OUT_OF_DOMAIN, settingOutOfDomainString.getStringValue());

		// initialise the ArrayList
		for (int i = 0; i < predictionColumnPositions.length; i++)
		{
			results.add(new Results(stringMap));
		}

		// Iterate through the table calculating the results
		RowIterator rows = input.iterator();
		int rowCounter = 0;
		while (rows.hasNext())
		{
			DataRow dataRow = rows.next();
			DataCell cell = dataRow.getCell(activityColumnIndex);

			// Set to true if the row should be added to the issues table
			List<String> errors = new ArrayList<>();
			
			if (cell.isMissing())
			{
				LOGGER.warn("Missing value has been ignored");
				errors.add("Missing value has been ignored");
			}
			else if (!targetValueCompatible(((StringCell) cell).getStringValue()))
			{
				LOGGER.warn("Target value not compatible with specified active and inactive strings");
				errors.add("Target value not compatible with specified active and inactive strings");
			}
			else
			{

				try
				{
					// For each prediction increment the result
					String activity = ((StringCell) cell).getStringValue();

					for (int i = 0; i < predictionColumnPositions.length; i++)
					{
						Results currentResult = results.get(i);
						try
						{
							
							DataCell predictionCell = dataRow.getCell(predictionColumnPositions[i]);

							if (!predictionCell.isMissing())
							{
								String prediction = ((StringCell) predictionCell).getStringValue();
								currentResult.increment(activity, prediction);
							}
							else
							{
								if (settingMissingValueAsOut.getBooleanValue())
								{
									// Treat as out of domain
									currentResult.increment(activity, null);
								}
								else
								{
									errors.add("Missing value found for column " + input.getSpec().getColumnSpec(predictionColumnPositions[i]).getName());
								}
							}
						}
						catch (Exception exception)
						{
							currentResult.incrementError();
							errors.add("Error calculating result when processing column " + input.getSpec().getColumnSpec(predictionColumnPositions[i]).getName() + ": " + exception.getMessage());
						}
					}
				}
				catch (Exception exception)
				{
					errors.add("Error processing row: " + exception.getMessage());
					getLogger().error("Failed to process row " + dataRow.getKey(), exception);
				}

			}

			// Add the row if it was missing
			if (!errors.isEmpty())
			{
				setWarningMessage("Problematic rows found, see second output table");
				
				DataRow errorRow = createErrorRow(dataRow, errors);
				containerIssues.addRowToTable(errorRow);
			}

			rowCounter++;
			exec.setProgress((double) rowCounter / input.size());
			exec.checkCanceled();
		}

		// Output results
		DataTableSpec outputSpec = getOutputDataTableSpec();
		BufferedDataContainer container = exec.createDataContainer(outputSpec);

		for (int i = 0; i < predictionColumnPositions.length; i++)
		{
			container.addRowToTable(createRow(new RowKey(includedColumns.get(i)), results.get(i)));
		}

		// Close containers
		container.close();
		containerIssues.close();

		return new BufferedDataTable[] { container.getTable(), containerIssues.getTable() };

	}

	private DataRow createErrorRow(DataRow dataRow, List<String> errors)
	{
		return new AppendedColumnRow(dataRow, StringCellFactory.create(errors.toString()));
	}

	private boolean targetValueCompatible(String stringValue)
	{
		boolean compatible = false;

		if (stringValue.equals(settingActiveString.getStringValue()))
			compatible = true;

		if (stringValue.equals(settingInactiveString.getStringValue()))
			compatible = true;

		return compatible;
	}

	/**
	 * Create a new row given the results and the RowKey
	 * 
	 * @param rowKey
	 *        The RowKey, this should be based on the column header
	 * @param results
	 *        The results for the given column
	 * 
	 * @return
	 */
	private DataRow createRow(RowKey rowKey, Results results)
	{
		DataCell[] cells = new DataCell[numOutCols];

		cells[0] = DoubleCellFactory.create(results.getBalancedAccuracy());
		cells[1] = DoubleCellFactory.create(results.getAccuracy());
		cells[2] = DoubleCellFactory.create(results.getSensitivity());
		cells[3] = DoubleCellFactory.create(results.getSpecificity());
		cells[4] = DoubleCellFactory.create(results.getPrecision());
		cells[5] = DoubleCellFactory.create(results.getNegativePredictivity());
		cells[6] = DoubleCellFactory.create(results.getRecall());
		cells[7] = DoubleCellFactory.create(results.getFMeasure());
		cells[8] = DoubleCellFactory.create(results.calculateMCC());
		cells[9] = DoubleCellFactory.create(results.calculateYoudensJStatistic());

		cells[10] = DoubleCellFactory.create(results.getBalancedPpv());
		cells[11] = DoubleCellFactory.create(results.getBalancedNpv());
		cells[12] = DoubleCellFactory.create(results.getBalancedMcc());

		cells[13] = IntCellFactory.create(results.getTotal());
		cells[14] = IntCellFactory.create(results.getCount(Result.TRUE_POSITIVE));
		cells[15] = IntCellFactory.create(results.getCount(Result.FALSE_POSITIVE));
		cells[16] = IntCellFactory.create(results.getCount(Result.TRUE_NEGATIVE));
		cells[17] = IntCellFactory.create(results.getCount(Result.FALSE_NEGATIVE));
		cells[18] = IntCellFactory.create(results.getCount(Result.EQUIVOCAL));
		cells[19] = IntCellFactory.create(results.getCount(Result.OUT_OF_DOMAIN));
		cells[20] = DoubleCellFactory.create(results.getCoverage());
		
		cells[21] = IntCellFactory.create(results.getErrorCount());

		return new DefaultRow(rowKey, cells);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset()
	{
		// Nothing to do
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException
	{
		return new DataTableSpec[] { getOutputDataTableSpec(), createErrorSpec(inSpecs[0]) };
	}

	private DataTableSpec createErrorSpec(DataTableSpec inSpec)
	{
		DataTableSpecCreator creator = new DataTableSpecCreator(inSpec);
		creator.addColumns(new DataColumnSpecCreator(DataTableSpec.getUniqueColumnName(inSpec, "Error cause"), StringCellFactory.TYPE).createSpec());
		
		return creator.createSpec();
	}

	/**
	 * Create the output specification based on the measures available from
	 * {@link Results}
	 * 
	 * @return
	 */
	private DataTableSpec getOutputDataTableSpec()
	{

		DataColumnSpec[] outColSpec = new DataColumnSpec[numOutCols];

		outColSpec[0] = new DataColumnSpecCreator("Balanced Accuracy", DoubleCellFactory.TYPE).createSpec();
		outColSpec[1] = new DataColumnSpecCreator("Accuracy", DoubleCellFactory.TYPE).createSpec();
		outColSpec[2] = new DataColumnSpecCreator("Sensitivity", DoubleCellFactory.TYPE).createSpec();
		outColSpec[3] = new DataColumnSpecCreator("Specificity", DoubleCellFactory.TYPE).createSpec();
		outColSpec[4] = new DataColumnSpecCreator("PPV", DoubleCellFactory.TYPE).createSpec();
		outColSpec[5] = new DataColumnSpecCreator("NPV", DoubleCellFactory.TYPE).createSpec();
		outColSpec[6] = new DataColumnSpecCreator("Recall", DoubleCellFactory.TYPE).createSpec();
		outColSpec[7] = new DataColumnSpecCreator("F-Measure", DoubleCellFactory.TYPE).createSpec();
		outColSpec[8] = new DataColumnSpecCreator("MCC", DoubleCellFactory.TYPE).createSpec();
		outColSpec[9] = new DataColumnSpecCreator("Youden's J Statistic", DoubleCellFactory.TYPE).createSpec();

		outColSpec[10] = new DataColumnSpecCreator("Balanced PPV", DoubleCellFactory.TYPE).createSpec();
		outColSpec[11] = new DataColumnSpecCreator("Balanced NPV", DoubleCellFactory.TYPE).createSpec();
		outColSpec[12] = new DataColumnSpecCreator("Balanced MCC", DoubleCellFactory.TYPE).createSpec();

		outColSpec[13] = new DataColumnSpecCreator("Total", IntCellFactory.TYPE).createSpec();
		outColSpec[14] = new DataColumnSpecCreator("TP", IntCellFactory.TYPE).createSpec();
		outColSpec[15] = new DataColumnSpecCreator("FP", IntCellFactory.TYPE).createSpec();
		outColSpec[16] = new DataColumnSpecCreator("TN", IntCellFactory.TYPE).createSpec();
		outColSpec[17] = new DataColumnSpecCreator("FN", IntCellFactory.TYPE).createSpec();
		outColSpec[18] = new DataColumnSpecCreator("Equivocal", IntCellFactory.TYPE).createSpec();
		outColSpec[19] = new DataColumnSpecCreator("Out of Domain", IntCellFactory.TYPE).createSpec();
		outColSpec[20] = new DataColumnSpecCreator("Coverage", DoubleCell.TYPE).createSpec();
		
		outColSpec[21] = new DataColumnSpecCreator("Error count", IntCellFactory.TYPE).createSpec();

		return new DataTableSpec(outColSpec);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings)
	{
		settingSelectedPredictionColumns.saveSettingsTo(settings);
		settingActiveString.saveSettingsTo(settings);
		settingInactiveString.saveSettingsTo(settings);
		settingEquivocalString.saveSettingsTo(settings);
		settingOutOfDomainString.saveSettingsTo(settings);
		settingActivityColumn.saveSettingsTo(settings);
		settingMissingValueAsOut.saveSettingsTo(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		settingSelectedPredictionColumns.loadSettingsFrom(settings);
		settingActiveString.loadSettingsFrom(settings);
		settingInactiveString.loadSettingsFrom(settings);
		settingEquivocalString.loadSettingsFrom(settings);
		settingOutOfDomainString.loadSettingsFrom(settings);
		settingActivityColumn.loadSettingsFrom(settings);
		settingMissingValueAsOut.loadSettingsFrom(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		settingSelectedPredictionColumns.validateSettings(settings);
		settingActiveString.validateSettings(settings);
		settingInactiveString.validateSettings(settings);
		settingEquivocalString.validateSettings(settings);
		settingOutOfDomainString.validateSettings(settings);
		settingActivityColumn.validateSettings(settings);
		settingMissingValueAsOut.validateSettings(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{
		// Nothing to do
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{
		// Nothing to do
	}

	///////////////////////////
	///// Streaming API

	/** {@inheritDoc} */
	@Override
	public InputPortRole[] getInputPortRoles()
	{
		return new InputPortRole[] { InputPortRole.NONDISTRIBUTED_STREAMABLE };
	}

	/** {@inheritDoc} */
	@Override
	public OutputPortRole[] getOutputPortRoles()
	{
		return new OutputPortRole[] { OutputPortRole.NONDISTRIBUTED, OutputPortRole.NONDISTRIBUTED };
	}

	/** {@inheritDoc} */
	@Override
	public StreamableOperator createStreamableOperator(final PartitionInfo partitionInfo,
			final PortObjectSpec[] inSpecs) throws InvalidSettingsException
	{
		return new StreamableOperator()
		{

			@Override
			public StreamableOperatorInternals saveInternals()
			{
				return null;
			}

			@Override
			public void runFinal(final PortInput[] inputs, final PortOutput[] outputs, final ExecutionContext ctx)
					throws Exception
			{
				RowInput in = (RowInput) inputs[0];
				// Create the list to buffer the results
				results = createResultsList(in.getDataTableSpec());
				RowOutput out = (RowOutput) outputs[0];
				RowOutput out1 = (RowOutput) outputs[1];
				MultiPerformanceNodeModel3.this.execute(in, out1, ctx);

				// Now that all the results have been collected we can calculate
				// the performance of the model
				// and then push the results as each model been calculated
				for (int i = 0; i < getPredictionColumnPositions(in.getDataTableSpec()).length; i++)
					out.push(createRow(new RowKey(settingSelectedPredictionColumns.getIncludeList().get(i)),
							results.get(i)));

				out.close();
				// Clear this from memory?
				results.clear();
			}
		};
	}

	protected void execute(RowInput inData, RowOutput outData, ExecutionContext ctx) throws InterruptedException
	{
		DataRow row;
		while ((row = inData.poll()) != null)
		{
			String activityColumnName = settingActivityColumn.getColumnName();
			int activityColumnIndex = inData.getDataTableSpec().findColumnIndex(activityColumnName);

			DataCell cell = row.getCell(activityColumnIndex);

			int[] predictionColumnPositions = getPredictionColumnPositions(inData.getDataTableSpec());

			// Set to true if the row should be added to the issues table
			List<String> errors = new ArrayList<>();

			if (cell.isMissing())
			{
				LOGGER.warn("Missing target value has been ignored");
				errors.add("Missing target value has been ignored");
			}
			else
			{
				String activity = ((StringCell) cell).getStringValue();

				for (int i = 0; i < predictionColumnPositions.length; i++)
				{
					Results currentResult = results.get(i);
					DataCell predictionCell = row.getCell(predictionColumnPositions[i]);

					
					try
					{
						if (!predictionCell.isMissing())
						{
							String prediction = ((StringCell) predictionCell).getStringValue();
							currentResult.increment(activity, prediction);
						}
						else
						{
							if (settingMissingValueAsOut.getBooleanValue())
							{
								// Treat as out of domain
								currentResult.increment(activity, null);
							}
							else
							{
								errors.add("Missing value found for column " + inData.getDataTableSpec().getColumnSpec(predictionColumnPositions[i]).getName());
							}
						}
					}						
					catch (Exception exception)
					{
						currentResult.incrementError();
						errors.add("Error calculating result when processing column " + inData.getDataTableSpec().getColumnSpec(predictionColumnPositions[i]).getName() + ": " + exception.getMessage());
					}
				}

				if (!errors.isEmpty())
				{
					// We push the error out immediately
					outData.push(createErrorRow(row, errors));
				}
			}

		}
		inData.close();
		outData.close();
	}

	private int[] getPredictionColumnPositions(DataTableSpec inDataSpec)
	{
		/** the index positions of the prediction columns selected */
		int[] predictionColumnPositions = new int[settingSelectedPredictionColumns.getIncludeList().size()];
		for (int i = 0; i < predictionColumnPositions.length; i++)
		{
			predictionColumnPositions[i] = inDataSpec
					.findColumnIndex(settingSelectedPredictionColumns.getIncludeList().get(i));
		}

		return predictionColumnPositions;
	}

	private List<Results> createResultsList(DataTableSpec inDataSpec)
	{

		/** the index positions of the prediction columns selected */
		int[] predictionColumnPositions = getPredictionColumnPositions(inDataSpec);

		// Create the map of the predicted values to the enum values
		// This can then be passed into the constructor for the results
		// that need to be created per column
		/** the array of results for the columns selected */
		List<Results> results = new ArrayList<Results>();
		Map<Result, String> stringMap = new TreeMap<Result, String>();
		stringMap.put(Result.EQUIVOCAL, settingEquivocalString.getStringValue());
		stringMap.put(Result.ACTIVE, settingActiveString.getStringValue());
		stringMap.put(Result.INACTIVE, settingInactiveString.getStringValue());
		stringMap.put(Result.OUT_OF_DOMAIN, settingOutOfDomainString.getStringValue());

		// initialise the ArrayList
		for (int i = 0; i < predictionColumnPositions.length; i++)
		{
			results.add(new Results(stringMap));
		}

		return results;
	}

}
