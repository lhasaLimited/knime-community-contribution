/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.multiperformance3;

import java.util.ArrayList;

import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnFilter;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentString;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.knime.core.node.defaultnodesettings.SettingsModelString;


/**
 * <code>NodeDialog</code> for the "MultiPerformance" Node.
 * Calculate the performance of multiple models
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Lhasa Limited
 */
public class MultiPerformanceNodeDialog3 extends DefaultNodeSettingsPane 
{
	
	private final SettingsModelString settingActiveString;
	private final DialogComponentString componentActiveString;	
	private final SettingsModelString settingInactiveString;
	private final DialogComponentString componentInactiveString;	
	private final SettingsModelString settingEquivocalString;
	private final DialogComponentString componentEquivocalString;
	private final SettingsModelString settingDomainString;
	private final DialogComponentString componentDomainString;
	
	
	private final SettingsModelColumnName settingActivityColumn;
	private final DialogComponentColumnNameSelection componentActivityColumn;

	// Prediction columns
	private final SettingsModelFilterString settingPredictionColumns;
	private final DialogComponentColumnFilter componentPredictionColumnSelection;
	
	
	
    @SuppressWarnings("unchecked")
	protected MultiPerformanceNodeDialog3() 
    {

    	// First we create the various components
    	// This is currently done seperate as there is an intention to
    	// allow for the auto population of the values based on the domain
    	// of the column selected. However if configuring when not all values
    	// are present in the table this isn't available using default dialog
    	// components
    	
    	settingPredictionColumns = new SettingsModelFilterString(MultiPerformanceNodeModel3.CONFIG_PREDICTION_COLUMNS, new ArrayList<String>(), new ArrayList<String>());
		componentPredictionColumnSelection = new DialogComponentColumnFilter(settingPredictionColumns, 0, true, StringValue.class);
		
		settingActivityColumn = new SettingsModelColumnName(MultiPerformanceNodeModel3.CONFIG_ACTIVITY_COLUMN,"");
    	componentActivityColumn = new DialogComponentColumnNameSelection(settingActivityColumn, "Activity: ", 0, StringValue.class); 
    	
		settingActiveString = new SettingsModelString(MultiPerformanceNodeModel3.CONFIG_ACTIVE_STRING,"active");
    	componentActiveString = new DialogComponentString(settingActiveString, "Activity string:");
    	
    	settingInactiveString = new SettingsModelString(MultiPerformanceNodeModel3.CONFIG_INACTIVE_STRING,"inactive");
    	componentInactiveString = new DialogComponentString(settingInactiveString, "Inactivity string:");
		
    	settingEquivocalString = new SettingsModelString(MultiPerformanceNodeModel3.CONFIG_EQUIVOCAL_STRING,"equivocal");
    	componentEquivocalString = new DialogComponentString(settingEquivocalString, "Equivocal string:");
		
    	settingDomainString = new SettingsModelString(MultiPerformanceNodeModel3.CONFIG_OUT_OF_DOMAIN_STRING,"out of domain");
    	componentDomainString = new DialogComponentString(settingDomainString, "Out of domain string:");
    	

    	// Add the components
		createNewGroup("Settings");
    	setHorizontalPlacement(true);
    	super.addDialogComponent(componentActiveString);
    	super.addDialogComponent(componentInactiveString);
    	
    	setHorizontalPlacement(false);
    	setHorizontalPlacement(true);
    	super.addDialogComponent(componentEquivocalString);
    	super.addDialogComponent(componentDomainString);
    	
    	setHorizontalPlacement(false);

		super.addDialogComponent(componentActivityColumn);
		
    	createNewGroup("Predictions");           
    	super.addDialogComponent(componentPredictionColumnSelection);
    	addDialogComponent(new DialogComponentBoolean(new SettingsModelBoolean(MultiPerformanceNodeModel3.CONFIG_MISSING_VALUE, true), "Missing as out of domain"));
    }
    
    
}

