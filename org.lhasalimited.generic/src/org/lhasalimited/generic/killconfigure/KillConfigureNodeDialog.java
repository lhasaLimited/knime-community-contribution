package org.lhasalimited.generic.killconfigure;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;

/**
 * <code>NodeDialog</code> for the "KillConfigure" Node.
 * 
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more
 * complex dialog please derive directly from
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Samuel, Lhasa Limited
 */
public class KillConfigureNodeDialog extends DefaultNodeSettingsPane
{

	/**
	 * New pane for configuring the KillConfigure node.
	 */
	protected KillConfigureNodeDialog()
	{
		SettingsModelBoolean settingKillConfigure = new SettingsModelBoolean(KillConfigureNodeModel.CONFIG_KILL_CONFIGURE, true);
		
		addDialogComponent(new DialogComponentBoolean(settingKillConfigure, "Kill configure"));

	}
}
