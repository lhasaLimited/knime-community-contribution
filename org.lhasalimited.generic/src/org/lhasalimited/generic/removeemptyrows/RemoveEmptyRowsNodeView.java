package org.lhasalimited.generic.removeemptyrows;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "RemoveEmptyRows" Node.
 * Removes empty rows from the input table
 *
 * @author Samuel, Lhasa Limited
 */
public class RemoveEmptyRowsNodeView extends NodeView<RemoveEmptyRowsNodeModel> {

    /**
     * Creates a new view.
     * 
     * @param nodeModel The model (class: {@link RemoveEmptyRowsNodeModel})
     */
    protected RemoveEmptyRowsNodeView(final RemoveEmptyRowsNodeModel nodeModel) {
        super(nodeModel);
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void modelChanged() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onClose() {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void onOpen() {
        // TODO: generated method stub
    }

}

