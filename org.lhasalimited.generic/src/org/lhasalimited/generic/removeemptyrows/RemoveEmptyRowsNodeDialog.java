package org.lhasalimited.generic.removeemptyrows;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;

/**
 * <code>NodeDialog</code> for the "RemoveEmptyRows" Node.
 * Removes empty rows from the input table
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Samuel, Lhasa Limited
 */
public class RemoveEmptyRowsNodeDialog extends DefaultNodeSettingsPane {

    /**
     * New pane for configuring the RemoveEmptyRows node.
     */
    protected RemoveEmptyRowsNodeDialog() 
    {
    	RemoveEmptyRowsSettings settings = new RemoveEmptyRowsSettings();
    	
    	addDialogComponent(settings.createDialogComponentAllMissing());
    	addDialogComponent(settings.createDialogComponentNoColumns());

    }
}

