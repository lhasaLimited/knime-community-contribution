package org.lhasalimited.generic.htmltotable2;

import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.knime.core.node.defaultnodesettings.SettingsModelInteger;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.lhasalimited.generic.node.config.NodeSettingCollection;

public class TableToHtmlString2Settings extends NodeSettingCollection 
{
	
	public static String CFG_COLUMN_SELECTION = "COLUMN_SELECTION";
	protected static String CFG_NUM_ROWS = "NUM_ROWS";

	
	protected static String CFG_INLCUDE_ROWID = "INCLUDE_ROWID";
	protected static String CFG_DOUBLE_FORMAT = "DOUBLE_FORMAT";
	protected static String CFG_FORMAT_DOUBLE = "FORMAT_DOUBLE";
	protected static String CFG_ADD_BASIC_STYLE = "ADD_BASIC_STYLE";
	
	
	protected static String CFG_STYLE_TABLE = "cfgStyleTable";
	protected static String CFG_STYLE_HEADER_TR = "cfgStyleHeaderTr";
	
	protected static String CFG_STYLE_TR = "cfgStyleTr";
	
	protected static String CFG_STYLE_TD = "cfgStyleTd";
	protected static String CFG_STYLE_TR_EVEN_ROW = "cfgStyleTrEven";
	protected static String CFG_STYLE_TR_ODD_ROW = "cfgStyleTrOdd";
	
	protected static String CFG_STYLE_MISSIG = "cfgStyleMissing";
	
	@Override
	protected void addSettings() 
	{
		addSetting(CFG_COLUMN_SELECTION, new SettingsModelFilterString(CFG_COLUMN_SELECTION));
		addSetting(CFG_NUM_ROWS, new SettingsModelInteger(CFG_NUM_ROWS, 1000));
		addSetting(CFG_INLCUDE_ROWID, new SettingsModelBoolean(CFG_INLCUDE_ROWID, true));
		addSetting(CFG_DOUBLE_FORMAT, new SettingsModelString(CFG_DOUBLE_FORMAT, "#.00"));
		addSetting(CFG_FORMAT_DOUBLE, new SettingsModelBoolean(CFG_FORMAT_DOUBLE, true));
		addSetting(CFG_ADD_BASIC_STYLE, new SettingsModelBoolean(CFG_ADD_BASIC_STYLE, true));
		
		addSetting(CFG_STYLE_TABLE, new SettingsModelString(CFG_STYLE_TABLE, "border:1px solid black"));
		
		addSetting(CFG_STYLE_HEADER_TR, new SettingsModelString(CFG_STYLE_HEADER_TR, "background-color: black; color: white;"));
		addSetting(CFG_STYLE_TD, new SettingsModelString(CFG_STYLE_TD, "border: 1px solid #ddd;"));
		addSetting(CFG_STYLE_TR, new SettingsModelString(CFG_STYLE_TR, "padding: 0px;"));
		addSetting(CFG_STYLE_TR_ODD_ROW, new SettingsModelString(CFG_STYLE_TR_ODD_ROW, ""));
		addSetting(CFG_STYLE_TR_EVEN_ROW, new SettingsModelString(CFG_STYLE_TR_EVEN_ROW, "background-color: #f2f2f2;"));
		addSetting(CFG_STYLE_MISSIG, new SettingsModelString(CFG_STYLE_MISSIG, "color: red;"));
	}
	
	/**
	 * The column selection setting
	 * @return
	 */
	public SettingsModelFilterString getColumnFilter()
	{
		return getSetting(CFG_COLUMN_SELECTION, SettingsModelFilterString.class);
	}
	
	/**
	 * The number of rows to output
	 * @return
	 */
	public int getNumRows()
	{
		return getInteger(CFG_NUM_ROWS);
	}
	
	/**
	 * Whether RowId's should be used
	 * @return
	 */
	public boolean getIncludeRowId()
	{
		return getBooleanValue(CFG_INLCUDE_ROWID);
	}
	
	/**
	 * The format to provide to the double formatter
	 * @return
	 */
	public String getDoubleFormat()
	{
		return getString(CFG_DOUBLE_FORMAT);
	}
	
	/**
	 * Whether the double formatter should be used
	 * @return
	 */
	public boolean isFormatDouble()
	{
		return getBooleanValue(CFG_FORMAT_DOUBLE);
	}

	/**
	 * Whether inline CSS styles should be added
	 * @return
	 */
	public boolean isAddStyle()
	{
		return getBooleanValue(CFG_ADD_BASIC_STYLE);
	}

	/**
	 * Get the CSS string for the table element
	 * @return
	 */
	public String getStyleTable()
	{
		return getString(CFG_STYLE_TABLE);
	}

	/**
	 * Get the CSS style for the TR elements in the header
	 * @return
	 */
	public String getStyleHeaderTr()
	{
		return getString(CFG_STYLE_HEADER_TR);
	}
	
	/**
	 * Get the CSS style for the TD elements in the header
	 * @return
	 */
	public String getStyleTd()
	{
		return getString(CFG_STYLE_TD);
	}
	
	/**
	 * Get the additional styles for missing cells
	 * @return
	 */
	public String getStyleMissing()
	{
		return getString(CFG_STYLE_MISSIG);
	}
	
	/**
	 * Additional style for even data rows
	 * @return
	 */
	public String getStyleTrEven()
	{
		return getString(CFG_STYLE_TR_EVEN_ROW);
	}
	
	/**
	 * Additional style for odd data rows
	 * @return
	 */
	public String getStyleTrOdd()
	{
		return getString(CFG_STYLE_TR_ODD_ROW);
	}

	/**
	 * The style for data rows
	 * @return
	 */
	public String getStyleTr() 
	{
		return getString(CFG_STYLE_TR);
	}
}

