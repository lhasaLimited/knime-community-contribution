/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.util;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Calculates a number of performance metrics for a set of binary
 * classifications.
 * 
 * Not thread safe 
 * 
 * You can either create the object with the appropriate counts of increment
 * values.
 * 
 * @author Samuel
 * 
 */
public class Results
{
	
	// 09/03/2020 what problem was happening that everything is synchronized? Is it so it works with streaming? Should the class not be synchronized?
 
	
	// Various counts
	private int truePositives;
	private int falsePositives;
	private int trueNegatives;
	private int falseNegatives;
	private int equivocals;
	private int outOfDomains;
	private int error;
	
	/** supports the calculation of TP, FP, TN, FN. Stores the link between the
	 *  the result type and the string representation
	 * */
	private Map<Result, String> stringMap;

	/**
	 * Create a result object with all counts set to 0
	 */
	public Results()
	{
		truePositives = 0;
		falsePositives = 0;
		trueNegatives = 0;
		falseNegatives = 0;
		equivocals = 0;
		outOfDomains = 0;
		
		stringMap = new TreeMap<Result, String>();
	}
	
	
	public Results(Map<Result, String> map)
	{
		truePositives = 0;
		falsePositives = 0;
		trueNegatives = 0;
		falseNegatives = 0;
		equivocals = 0;
		outOfDomains = 0;
		
		stringMap = map;
	}

	/**
	 * Create a new results object with given values for true positives, false
	 * positives, true negatives and false negatives. Out of domain and
	 * equivocals are set to 0.
	 * 
	 * @param tp
	 *            true positive count
	 * @param fp
	 *            false positive count
	 * @param tn
	 *            true negative count
	 * @param fn
	 *            false negative count
	 */
	public Results(int tp, int fp, int tn, int fn)
	{
		truePositives = tp;
		falsePositives = fp;
		trueNegatives = tn;
		falseNegatives = fn;
		equivocals = 0;
		outOfDomains = 0;
	}

	/**
	 * Create a new results object with given values for true positives, false
	 * positives, true negatives and false negatives.
	 * 
	 * @param tp
	 *            true positive count
	 * @param fp
	 *            false positive count
	 * @param tn
	 *            true negative count
	 * @param fn
	 *            false negative count
	 * @param equivocal
	 *            equivocal result count
	 * @param outOfDomain
	 *            out of domain result count
	 */
	public Results(int tp, int fp, int tn, int fn, int equivocal, int outOfDomain)
	{
		this.truePositives = tp;
		this.falsePositives = fp;
		this.trueNegatives = tn;
		this.falseNegatives = fn;
		this.equivocals = equivocal;
		this.outOfDomains = outOfDomain;
	}

		

	
	// Metrics
	/**
	 * Returns the total number of results
	 * 
	 * @return sum of TP, FP, TN, FN
	 */
	public synchronized  int getTotal()
	{
		return truePositives + trueNegatives + falsePositives + falseNegatives;
	}

	/**
	 * A representation of the correct predictions: TP + FP / Total.
	 * 
	 * This method can be misleading with imbalance in the target class
	 * 
	 * @return
	 */
	public synchronized double getAccuracy()
	{
		return ((double) truePositives + trueNegatives) / getTotal();
	}

	/**
	 * Otherwise known as true positive rate. TP/(TP + FN)
	 * 
	 * @return
	 */
	public synchronized  double getSensitivity()
	{
		return (double) truePositives / (truePositives + falseNegatives);
	}

	/**
	 * Otherwise known as true negative rate. TN / (TN + FP)
	 * 
	 * @return
	 */
	public synchronized  double getSpecificity()
	{
		return (double) trueNegatives / (trueNegatives + falsePositives);
	}

	/**
	 * This method gives a more realistic representation accounting for
	 * imbalance in the target class. It is the mean of the sentivity and the
	 * specificity.
	 * 
	 * @return
	 */
	public synchronized  double getBalancedAccuracy()
	{
		return (getSensitivity() + getSpecificity()) / 2;
	}

	/**
	 * Also known as positive predictivity: TP / (TP + FP)
	 * 
	 * @return
	 */
	public synchronized  double getPrecision()
	{
		return (double) truePositives / (truePositives + falsePositives);
	}

	/**
	 * TP / (TP + FN)
	 * 
	 * @return
	 */
	public synchronized  double getRecall()
	{
		return (double) truePositives / (truePositives + falseNegatives);
	}

	/**
	 * 2 * ((precision * recall) / (precision + recall)) <br>
	 * <br>
	 * In statistical analysis of binary classification, the F1 score (also
	 * F-score or F-measure) is a measure of a test's accuracy. It considers
	 * both the precision p and the recall r of the test to compute the score: p
	 * is the number of correct positive results divided by the number of all
	 * positive results, and r is the number of correct positive results divided
	 * by the number of positive results that should have been returned. The F1
	 * score can be interpreted as a weighted average of the precision and
	 * recall, where an F1 score reaches its best value at 1 and worst score at
	 * 0. <a href="http://en.wikipedia.org/wiki/F1_score">F1 score ~
	 * Wikipedia</a>
	 * 
	 * 
	 * @return
	 */
	public synchronized  double getFMeasure()
	{
		return 2 * ((getPrecision() * getRecall()) / (getPrecision() + getRecall()));
	}
	
	/**
	 * Calculate the Matthews correlation coefficient / Karl Pearson's phi coefficient. 
	 * <br><br>
	 * The MCC is bounded between -1 and 1 with +1 representing perfect prediction, 0 random and -1 total disagreement. 
	 * <br><br>
	 * <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAa8AAABDCAYAAAArmopMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABxKSURBVHhe7Z0JtFVTGICtZa0QmS3JnJmUuUjGMmQsFFIoJEqUSDItU4ZUJFNIiDRokAZTypSEkFTmMkemWCvDsb797v/6726fc8+979169/m/tc669+yzz977/Gef/e/h33uvFhmGYRhGiWHKyzAMwyg5THkZhmEYJYcpL8MwDKPkMOVlGIZhlBymvAzDMIySw5SXYRiGUXKY8jIMwzBKDlNehmEYRslhysswDMMoOUx5GYZhGCWHKS+jpJk3b170wgsvRCNHjoyefPLJ4PH0009nfEfB66NGjYomTZoU/fTTTxlfxePff/916R07dmwwLU888YQ7XnrpJef//fffj6ZPn+6O8ePHu/uFmTNnRjNmzCg/nnrqqcyVyoW0kOakY/78+c4v6Xv++edXOMTfP//84/wVE52u0PHiiy9mfC7PD+PGjcuS7cKFC518Od58882se4yqgSkvo6S56KKLog033DA6+eSTo44dO0ZXX311tNpqq0VXXXVVdMEFF0R77723O5dCk+uXX365c+vUqVPUpUuX6LLLLouaNGkSbbPNNq4QKyYDBgyIatWqFR1yyCFR+/btXTpJC+m69NJLoyOPPNKdU8gC7ieddFJUp04d546yFnjmevXqued//fXXo+HDh2euVC6kpV27di7+7bff3sV7/vnnu7QdfPDBzh0FJX55Jp5v//33jy688EL3XPo5i6nAUDLEhUyIj7R26NDBvXNkJWmADz74IGrdunW06667OvehQ4c6d5gwYYJLO+5t27Ytv8eoOpjyMkoWasr169ePpk2bVn7euHHjaJNNNikvIP/++29XAOkCs0ePHs7tlVdeybiUsdtuu0Vnnnlm0QpX0rfllltGU6dOzbhETgmQljlz5rhz4t59992jn3/+2Z3DsGHDnGLGX//+/TOuZVAw03IsNj/88IOL/8Ybb8y4lIF8UWiibOGTTz5xfvv06ZNxKePBBx90FYS//vor41IcaDURP4pJt6Zgu+22c61eAXnvtdderkJx+umnZ/mnooBy9sMwqgamvIyS5bXXXotatWpVXrhQEFEI0aISNwrKiRMnlp/ze+yxx0ZbbLFFlpLCvW7duq5wpUAOIWEUCvEdeuih5fESHoq2YcOG5WGHlC0ttIcffti5H3XUUeV++W3Tpk1Wa6xYPPbYYy5+utg0pIFWlk7DPffc4/yOGDEi41LGoEGDnDtdoSHkuUIkXfNBORHP3XffnXFZzo477uiUq8B/5EvLskaNGq41JtDaomVvVE1MeRkly+23355V46clRaE1ZMiQjEsUvfPOO1k1apTCOuus4worXSB+9NFH7t677rorWFDSFUVXkn+Nc2rnabrsaHH169cvcxa5cSLi7Nq1a8alLH26ts/vVlttFb377rvuOfAvigK/nGtFVywuvvhiF9d7773nzhljvOaaa1z6/DScccYZzk0rCfx17tw5atSoUTC9KDQqDqFrVEBoFacFeRI/7x7o0mzWrJkLG0Wr46A1eOutt0Yvv/yyu6dnz57OnfQedthhWa00o2physsoWShgOARqyrrQCoHhA3769u2bcSlTAueee2504IEHRt98803GNRviYfyErjCJk/tQahhY6HTE4acXBUBakgpIFABdo9wnab/jjjvctbfffjs6/PDDU8VdEQh/n332cQoERULrkPGjULrxS7enVhK48Z+0I6+49NKqo0WsW748M+NXWuEkQdj77bdf1KBBg2jZsmUuLCoDtAZ98EslhgoQ4e+8885Ogf7222/uvrXXXjv6888/M76NqoYpL6NaQOFD7ZoCMmlMRRQchWjv3r1dF+Iee+zhurRyFZBcv+SSS9y4Cf8Z0O/WrVvO+0Jwj6QlKb0oWTEW4B66HVFY/Gfs7oYbbnDXiom0SlFI++67ryvU119//Wjx4sUZH8uRbk/8ImPG9OgapdVIl1wuWVERaNGihfNHZQGlmSQfn99//911/xE/Sou4SY8YlGiIg2dZsmSJOxdDnoceeiiaO3euU4BxitZY9ZjyMqoFtJjWWmstV+DFFTi4H3300eUtCA4KMI60hRT+6Frq1auXK+gKLdy4j4IZY4GkME444QTXQhOwViRe0n7MMce4cb8QdMM999xzqY5cPPDAAy7OwYMHO1nRUgkZQ4CMNw0cOLBctii0fOTUtGlTp7Br167t7s+HKVOmuPjvv/9+dy+tLxRUKJzZs2e7yoCkDVlyL3mEMT66pY2qiykvo1rAmBMFT6h7SKAAo1soScHlgvsqQ3l9/fXX7v5zzjkn47IihE0LR+ZQweeffx7VrFnTFcqYz8e1ShibQsnkOkItEg0ya9mypUurjHdhjs54VwieB7+zZs3KuOTPmDFjXCsI45R8lRfjV8Qv44LMUaNLOPSeGH/U1pP4obXG/aeeeuoK1qhG1cKUl1EtkDk5UsCG4Bp+MMooBApS3W2IEtPGFflA1xRp0XOLfBh78cPnP60elAetk3wL93whfJ6XLrRccZE2WrUchaYLo5jmzZu7+/PtNiR+lI6eKhEHfjHx9xUUrS3eyxprrJE19mZUPUx5GSUPBRFGDbkKTSmYCpmITBxMFvYNNrBIw4pO3NIiLRTmT8VBKxKzfx8MG7iXrrFiI3O2ZNwtCeSBX8a5CoFVTpgSoJUVBhv+tIY4JP40FQr8MjnZD1eelzGzfN+psXKpkPLixXNQQ/EzgQ8ZIckv1/mQ6Xdm0uWXX37p/OFON0UoI/3666/RW2+95bo/GGAVP5yH/BvVC7oKKVSpoUuBw38ODV1HFKgUgpjJY1WYpjDW0NqJM5WnBo/JdS4Yh2ISNK0D0svBmJbfjYlhgyg3WhF+WvkuuDZ58uSMS+VDHLTukClxnXLKKS6dsmyVD8YZ4pdVTTjPB7pDzzvvvGDZgDJj8nncN427TivKC2OcuLTyLPjjwJJR+yMs7s83fxgrnwopLzKIWPNwxI03kCEZmBZ/HDojcv3OO+90A+5MOmW5HvwwPsFHHOo6YR035r+ceOKJbvkZ/NK9wSA2qxiEPgKjekHlBes0WZ+Og3UMmbOjWbBggXPX/j799NPM1fToPFgI3K/TwMFEXipbGgpT8veiRYvc4ZvvE87KqKBROZC1FkkryjIuTv+5ClmqKul5cj0r8en4R48eHbseIbJLSif3WflR9amQ8iJD8ZJRLgws69n/mmeeeaZ8gJtalM4Y/Mf0l5nv+oPkl9oP92jlhX/iwR1lqcMiM+LOod0NwzCM6kWFx7xQKjTX6fdHaXz44YeZK2VwnX57WkdcX7p0aeZK2TWZW/HII49kXJfDdRSXVl4oLPwz4z9E9+7dTXkZhmFUcypNebFtAErj5ptvzlwpA4spuvdYg43retsJukNwY9BblJPPo48+Wq68UEisVM24BWNiIT777DNTXoZhGNWcSlNedAcy+HzAAQdkKQ4m+7EKgAxSM79FYI4Jbsy+TwIrL+JhqRZMWFFgccoOUHamvAzDMKovlaq8ZMxJBkq5xv5EKBKsrLhGy0gQ02UMNNIgrbuKrPRMGrGESnOYxZFhGEbVpFKUl2zzgALDbJgJo7hjgMGMd2CWO4qHddIEJovillZJsLI2/pNWJcgF6SKt/iEm/L5bEpj32mGHHXbYUblHGipVefEfxbXZZpu5zfQwXZcVvkMrIGAOnEt5MbZFy46wZUkd1h7jPA78JF2vLJg7tPnmm9thhx122FGJRxoqRXlhcIHyAsziUR4s5skK1KJEWFYH95kzZ7pzYIIxxhdsYSD3+2CNyKRCaQ0xX4yFNvVeQRq2Q2drg7jwmABKizDNkWvdN8MwDGPVUOnKi1/282Eult7JVEzi9Vpi3Cum76FdTwmL1puedDp+/Hjnn9ZaSEGxEZ5v8ajJZ7VtU16GYRhVkwopLwp3DvZDwhCC1goKiZWaaXWhXDDewA/Lt6B0mK3PubTI+JUtzlFIdCvixniTrK6hlRTXCAP/zB179dVX3XUOFGHcbqyGYRhG9aFCyktWtuYQ6zyUy48//uiu8R/lJde0X64J/EcJsdTTtttu6xQQmwSyBl2cImL8C+VGKw9FxrJQ/P7yyy8ZH4ZhGEZ1pcLdhpUJSgxlRatLK7c48INff8mpUoPdX+2www477Pg9Uyrmpkopr/8jzF3baKONojXXXNMOO+yw4399sMp/Wkx5rUJoOXbp0mWFrdxxnzhxYubMEKZOnZq6RW7yC8OK9Wl6KZBh3JYi/3euvfbaVDJkaAMDMSNM2rwYhymvVQhz2LCO1AUyL/O+++5zc9tCcD3uoAtVd7ny61+ji5Vf3IqNjj/u8J9dDkmrdmPOIDsQ6Ht88MdYKGOrIXR4/iEyEvxr8ruyuqnTyE8Qv5JOLSP+y3tnB2cZm46D+1nFRstCSJMmfZ929+MUv3K9WKRJs6D9kjZ9iD8MzPxxex/8NmzY0Bmx+aRJD4cQchNwWxkylPCTDkG78axaTvwXebKZql/+5YMpr1UELwxrSd8cn32ndthhh6zMIFAAYZSCYmMqQsuWLcvP2S6d+W+cYywDLL/F7sK4sTjyLrvs4jYKZL8z7vHjrmwIn3iIHyMc0oIVqrhxMB8Q+MiZ83fQQQc5v+xyS1o5Z9UWpkhAx44d3ZzBuAzPxHEmxIfkJ9MsiB8rVkkHv3q/OSlwKOTxhxuGRPXq1XNzEjlnorxeLaYYUEA2atTIxcded0zeJA1NmjRZIa343XTTTZ3scNc7NPN85A/ceR7ZjDMkQ9y4FrfeKNcIh+kxyEA2diQOyWscMg+TdUmRXZ06ddzapzpO/JEvua+Y65HmK8fatWu7KTr4ZQNTjML0N8Mz8O0lybB58+bRgAEDMi7ZEAdxkq/JdzofahmKPHCvUaOGcwvJkGNlyFDSiWyQCTLF0lzSIO8cv5Q14u7nReSLO+lt0aKFm0YVkmMuTHmtIljqKtTqYmFjPZFbc++997pltrgHv1KQjB071p1TmyFzsBiyIAoPvyD3Uujgl3tywTSIQjIXEL7EL2Hwy/NTiOia6ffff+/6vMWvpJWuVdLKs3z88cdOyTFW6IPc2FcutLwMYXXo0CEaNGhQuayIhwLqq6++KncjrQsXLiy/h/Thxq+kiYNCrWfPnu6+JPArlreF8Oyzzwblx9xHnVbgvTdr1sy5Dx06NONaxpgxY1xNl3uRHQtcx8lQ1iP14V4KfC1DUe7857rkN30/czivuOIK5z5v3ryMaxlUROgOzgVhryw5yjvHL+CPQ74Znu2NN96I1ltvveDuFlw74ogjgmnlXmSInLQM6VHgnHuYC7vTTjtlyfD6669334YvQ/xTaYkrMzT4pcszlK40iFyYAyth8Ms57jq95EXZ4dzPiyhslgaEJDnmwpTXKoAXToGM0tHQMmDFfJ0JBO5BsU2fPr38nMxBLUb848aHQIYQ2J6eDKSVBNC1hnvcbrMaalKhNKVBFmvmV0NaWVZMLxdGJg9ldrbF0WmlEBMlrqGwQSYh2DkZWVFYAM9DrZEWnoSDGzuDix9ga/p111036/nxLzVkPw0+XKf1mMtfHDwT8cyaNSvjUgZppNDS6WKBa2r7G2+8sWs5SZz8Ii9WlxEIlzygwR+y7du3b8Ylm7lz52ZNXxF5IXOJCzfSK35wpxXCyje4UwgLXGvQoEGqggu/Otx8yUeOsuaqPxatvxnSw3fBDvAa3PF32223ZVyyQYZnn3228yfnhKnzMxU08oyWId/1TTfd5Pz279/fuQN+kGEauRBO/fr1y+PJl65du7r4v/3224xLGSJDHS7KW+bj+nmR3hf9zkNyTIMpr1UABSmtAP2yyXxssElhGYKWCrVquQf/dMW0atUqK2OwIzXb48v5WWed5VojOnPzn24fqUXmoiLKSzL8F1984c7ZVYACkvBw1+HS5Ycby4YJXKfgoTtMlAobl/pLgPEfxS/dkD64E47A+Blx6QJc0qTlScHA/EMdFy1EWmzyHEkQRqHKi7CppfJO5dkZs6IiwjXen04r3TgUrDwTtXTprpHn0mmNkyHjNHHdobTcRo0alTmLXMWDcPXqOLQK/HyKH9JPwUl3ElsbAd1MtPLSyAY//jOkhXtCcpT358uR7Zv4ZsQv4I9vBiUiaaAF2r59+6w0sV8h6UQphUCGLKEnSMVsyJAhGZeyFo6uhDFvVmTIr96xnu+KNKWRIemkYpPGrw/3kr/ocZDnRX6klXeux6L5xXKQ9DIE4udFrZghJMc0mPJaBbC/2ciRIzNnZfDiqM3H9ZOTIXSmkya8rsmCzgD8p2ZMLUi7UzhxL62yNBm5UOVF2GR4uqjIyJyjTKVlpcMUv2R0NjAV5IOlpSppnTFjhnNbvHixOwc+Ytyo4YfgXrkfqOnh328R6jRJoaGVHmHIkmazZ8/OuMZDeIUqrz/++MPFQ2GKHAhr6623Lq+1+mmVgoVxGe6jgAZq8shWp4HWOX64T6BigVLW4Wp8GZL3CEN6A4Dr+n6641AauEvrh3wH/N5yyy3ufy4Ik3vj0pZESI5UhmTXdx0m/2vVqpXqmxk3btwKCylQyaxZs2aWm0bLD2gtE67e5xA/2h8VBhZhwE3WiBXliPGIzp9J8OyMnflpSIN8XyhVno3WF93+ssWVfl7ylPRoyPORTuCbYdxapyEkxzSY8lrJUOts27btChmIF8dLTmviLQWB3/WokUIM5cN/+qG5j5obBU7azFKo8mLsifipzVMoSlcbLU8fGSshLhQzXQ7sAcfA8OOPP54VP8oNv1pRiTJfsGBBxiUewqK2iv+k7RdEfrL0Gdv7kCY+vrR99LznQpWXxE/tdYMNNnAGLLReKIR8qLl36tTJxcPz0WJklRrOURCMz2lonfsyxFgIw4Q07xo/FPCEwQ4ScdCyOO6449x/2dJIdoUgfWnGagD/3JsmbT4hOdJlHJKj/83QhRj3zdAFyXgNeVcYPXp0XjIkH/JOk/wff/zx5a1beiVIH92Scj95Mw34J22F5EXpAkSGtN4aN24cu6D68OHDXUsKZA9GDDOIt0+fPm78UxOSYxpMeVUyvKCkzIElnO56EcgEvOQ0e9kQPs10/OtWik+/fv2cHwZU+QA5qDkmfSh8CPpAOfbu3duF4V/LhYx30cqhOwXl2bRp06B8UNr4paCVtNK3T3eD759zLNikZQGkh/uJJxc8P0Yf1PaS3pXUcidNmlSeJmqJoUJP8GXEu0Z5URD613KBLIgfBUPNnHeH8gylGYst3YokrdxLWun25Bk0IkOpEQMFNYV6kkwE3gsyTOqywp3u8SlTprhz5I7iIl2MJ9HCiSuwfFmhFLiv2HIMfTN0sYa+GVof+NU7XOQjQ2kR0l0eB+FQ8ZM4RIZ0dS5btszdn0aGvAO2oKL7tBAZSpc+MqSMogelV69emavZIFum+4Ckd/XVV3fdzFRkdYUJvvvuuxXkmAZTXpXIkiVLXG0E67UQvEgyT+hDoJChn1jGq5LgoyFDS20mBO7Ufug2DMUXB90CcmBdxYFZcOvWrd14HN0AtDw4yHRJED+ZUnYSYDyGwiBEt27dnN80HxLPhuk3C0ALFIbcL+MpSVDbx29cWoA46Grzu9uSQM6+7MRCjUFrWkYoGWRHYZAkP8LCGpXuLXl/jDEw/cGH9NGKkXFFkBo6BRyy8hVuSIYU8hgFpckvWC8SPjXpOAjHN4ZgfIP7unfvHjvmipvOhyJP/J922mlOdmnzIWHlI8d8vpmlS5e6Z0EZCPnIkJYu92NFHAfh0PugwxMZ0iWrx6A0jMtq2XEwPo4SadeunatE0wKSvJgEcsE8HhnKt4AM9XMLXGfFIG2IJeklr5Ef/LwYkmMaTHlVEhMmTHCDvLyEOGstMhLjJSHIgIwNaGOFOKjNJ8UDhEc3HbWeUObOh0K6DXWGl3txk8yvwQ2lzoeYpusA/9T69SA3FQdkEldx0NAFg19tlelDLRA/SYVzGkhrId2GyIx5QFQacsmP62wAq98R/mjlUshgdu3fx7kvQ/Kwb6IdR48ePZx8WFA7Dioi2poTFi1a5LqIyBcosLQQhj/Qnwb85yNHjFjSfjNiKagLXYwx0spQjJmSZEgrhYpdSIa8WwxA0kKaKKNCz56EfAvIUCCsOBlSgdfX+CZJL0odZenfJ+OJprxWEYMHD3a1XF4Qfep+5uV8zz33XMFdwJ2uQMZ3ciGtlKRxF7EE4wOpKIUoL8nwmADngrAZi0C5p/mwQpmd2hz98bmMKAifD4j7k1q5YpQxYsSIjEthEF8hyks2ddVz9uJASfiTV0FMvmnx+YQK3vnz57vxjFzvmnjI50mbvoJ0u2m4l24l4vaNZZIQOeabD/ORI0oEv2m/mcmTJ8fKMNf75jqtemSY5Jd5WX6hrmWYz/JTyI4KYq60+VDBSStDhhlCPRpYwBJG6HsKyTENprwqGWkV+R8mBTPN5zjIWGTIuK4sMhxhUtMSwwdaXqEXTmHGOAd+sGxkALUi5KO8SKd0DxI/vwMHDoxdJ4+0UoMUv2m6DUXG2vAD5UU3atz9ki5kRq0fYwP++++JdCJTulmYikCXTpo0xUG8+Sgv5Ew6iZ9nRPZx748wqTTxPFh++XlBxmSGDRuWcVkO1q50j+mWLjKkyyeu9Us6kAVyI1y68YjflyH+MG6Je6ey0onu5syFyDFtPsxHjqD9YkWZRrEydoYMdTcYskOGfteYQKUSeUieR4aEg0GEBqXEWLPI0H+3jL1yLc0Yr4BMmKqQb16k7MolQ8Kkwoe1MOWTn14x3NAWwoLIMU2vi8aUVyXDC2csAQsrySS4JbW6BCzCxCQ2BB8Xy+2QmfTh+/ev86FUhHyV15VXXun61LGqlPTGPZOfVp4xF+yU7bdu+U9XVlxLj8JA5EffP4YErN/nDzqjvCQtknYG8QuF585HeWEIwXgOrSXGySQtofvnzJnj3g3TCBjH87ucuYeCL3QvMiR8X4ZYkWGgEIJuRWRIepAhtWnmJhKHhsF63GjB4N9/p8QTl6448JuP8qKQTCtHTLtlDA1zevyl+WaoGMbJUHfHajCQkbRoGfrx0eJCRoTPew3JMKkyHIJ7+G7Syj0uL4bguyEv8jz55sWQHNNgyqsIME5CLWPatGnuvE2bNqkyGgUsYxf51kCKDZkuria5siHzMz4WkidL/dStWzfvj6CYkF6UYNoCY2WAgQNjUf5KJoDiIr9WJRkC8iuk27BYMD2AFkZIhrhVRRmSHgxeqlJeRI5xeTEXpryKAHONmJjbuXNnV+jHWRj6kKlQFHG1G6PMyonaY0iZIj+WIoozijHKwPqOI5QnccNgAUs4I57rrrsuUYaMQYbWjjSySZJjLkx5FQEKUboEMCBgxYx81u2iUKbV5s/LMcoKBWSTNJEb+SF3vWKBsRzGMJBhUsHKihzIsJAC5f8Aa0QylyvJspVeFLqmq0qPRVUEOTKZPo2FcAhTXkVCLGhYziefQVWg0KAPmDENYzmMA6QxDabb1R8jMMpAhqzrmAvmzckqCUY25K00Bh34qeh4c3UmrRzjMOVVJGh9MW8raS6WYRiGURimvIoIXTRVaXDUMAyjumDKyzAMwyg5THkZhmEYJYcpL8MwDKPkMOVlGIZhlBymvAzDMIySw5SXYRiGUXKY8jIMwzBKDlNehmEYRslhysswDMMoOUx5GYZhGCVGFP0HMoo/eq1QXWkAAAAASUVORK5CYII="/>
	 * 
	 * @return
	 */
	public synchronized double calculateMCC()
	{
		double a = truePositives + falsePositives;
		double b = truePositives + falseNegatives;
		double c = trueNegatives + falsePositives;
		double d = trueNegatives + falseNegatives;
		
		
		return ((double) (truePositives * trueNegatives - falsePositives * falseNegatives)) / Math.sqrt(a * b * c * d);
	}
	
	/**
	 * sensitivity + specificity - 1
	 * @return
	 */
	public synchronized double calculateYoudensJStatistic()
	{
		return getSensitivity() + getSpecificity() - 1;
	}

	/**
	 * TN / (TN + FN)
	 * 
	 * @return
	 */
	public synchronized  double getNegativePredictivity()
	{
		return ((double) trueNegatives / ((double) trueNegatives + falseNegatives));
	}


	// Balanced metrics
	public synchronized double getBalancedPpv()
	{
		return getSensitivity() / ( getSensitivity() + 1 - getSpecificity());
	}
	
	public synchronized double getBalancedNpv()
	{
		return getSpecificity() / (getSpecificity() + 1 - getSensitivity());
	}
	
	/**
	 * Returns the balanced Mathews' correlation coefficient.
	 * 
	 * @return balanced Matthews' correlation coefficient
	 */
	public synchronized double getBalancedMcc()
	{	
		double result = 0;
		double numerator = getSensitivity() +  getSpecificity() - 1;
		
		double denominator;
		if ((getSensitivity() == 1 && getSpecificity() == 0) || (getSensitivity() == 0 && getSpecificity() == 1))
		{
			denominator = 1;			
		}
		else 
		{
			denominator = Math.sqrt(1 - Math.pow(getSensitivity() - getSpecificity(), 2));
		}
		
		result = numerator / denominator;
		
		return result;
	}	
	
	/**
	 * The coverage represents the proportion of predictions that arent out of domain.
	 * Equivocals are considered in domain
	 * @return
	 */
	public double getCoverage()
	{
		return  getTotal() / (double) (getTotal() + outOfDomains + equivocals);
	}
	
	
	// Supporting methods
	
	/**
	 * @param stringMap
	 * @return
	 */
	public synchronized void setMap(Map<Result, String> stringMap)
	{
		this.stringMap = stringMap;
	}

	/**
	 * @param activity
	 * @param prediction
	 */
	public synchronized void increment(String activity, String prediction)
	{
		incrementType(calculateType(activity, prediction));
	}
	
	
	
	/**
	 * Get the count for a specific type e.g. TRUE_NEGATIVE, TRUE_POSITIVE etc...
	 * @param type
	 * @return
	 */
	public synchronized int getCount(Result type)
	{
		int count = -1;
		
		switch (type) 
		{
		
			case TRUE_NEGATIVE : 	count = trueNegatives;
									break;
								
			case TRUE_POSITIVE :	count = truePositives;
									break;
		
			case FALSE_POSITIVE :	count = falsePositives;
									break;
									
			case FALSE_NEGATIVE : 	count = falseNegatives;
									break;
									
			case EQUIVOCAL		: 	count = equivocals;
									break;
									
			case OUT_OF_DOMAIN	:	count = outOfDomains;
									break;
									
			default				: 	throw new IllegalArgumentException();
																
		}
		
		return count;
		
	}
	
	/**
	 * Calculate the type given the activity and prediction
	 * @param activity				the known activity
	 * @param prediction			a null value is assumed to be out of domain
	 * @return
	 */
	public synchronized Result calculateType(String activity, String prediction) throws IllegalArgumentException
	{
		// Treat null as out of domain and don't validate the value
		if(prediction == null)
		{
			validateValues(List.of(activity));
			return Result.OUT_OF_DOMAIN;
		}
				
		validateValues(List.of(activity, prediction));
		
		Result type = null;
		
		if(prediction.equals(stringMap.get(Result.EQUIVOCAL)))
		{
			type = Result.EQUIVOCAL;
		}
		else if((prediction.equals(stringMap.get(Result.OUT_OF_DOMAIN))))
		{
			type = Result.OUT_OF_DOMAIN;
		} else
		{
			
			if(prediction.equals(activity))
			{
				type = prediction.equals(stringMap.get(Result.ACTIVE)) ? Result.TRUE_POSITIVE : Result.TRUE_NEGATIVE;
			} else
			{
				type = prediction.equals(stringMap.get(Result.INACTIVE)) ? Result.FALSE_NEGATIVE : Result.FALSE_POSITIVE;
			}
	
		}
		
		return type;
	}
	
	private void validateValues(List<String> values)
	{
		
		if(stringMap.isEmpty())
			throw new IllegalArgumentException("Value to class map has not been created");
		
		Set<String> invalidValues = values.stream().filter(this::valueMapMissingValue).collect(Collectors.toSet());
		
		if(!invalidValues.isEmpty())
			throw new IllegalArgumentException("Unknown classes found: " + invalidValues.toString());
			
	}
	
	private boolean valueMapMissingValue(String value)
	{
		return !stringMap.values().contains(value);
	}
	
	/**
	 * Increment the count of the given type.
	 * @param type
	 */
	public synchronized void incrementType(Result type)
	{
			switch (type) 
			{
			
				case TRUE_NEGATIVE : 	trueNegatives++;
										break;
									
				case TRUE_POSITIVE :	truePositives++;
										break;
			
				case FALSE_POSITIVE :	falsePositives++;
										break;
										
				case FALSE_NEGATIVE : 	falseNegatives++;
										break;
										
				case EQUIVOCAL		: 	equivocals++;
										break;
										
				case OUT_OF_DOMAIN	:	outOfDomains++;
										break;
										
				default				: 	throw new IllegalArgumentException("Type (" + type.toString() + ") not incrementable");
																	
			}
	}
	
	public void incrementError()
	{
		this.error++;
	}
	
	public int getErrorCount()
	{
		return this.error;
	}
	
	
	// Deprecated methods
	
	/**
	 * Increment the count of true positives
	 * @deprecated	replace with incrementType(Result type)
	 */
	public void incrementTruePositives()
	{
		truePositives++;
	}

	/**
	 * Increment the count of false positives
	 * @deprecated	replace with incrementType(Result type)
	 */
	public void incrementFalsePositives()
	{
		falsePositives++;
	}

	/**
	 * Increment the count of true negatives
	 * @deprecated	replace with incrementType(Result type)
	 */
	public void incrementTrueNegatives()
	{
		trueNegatives++;
	}

	/**
	 * Increment the count of false engatives
	 * @deprecated	replace with incrementType(Result type)
	 */
	public void incrementFalseNegatives()
	{
		falseNegatives++;
	}

	/**
	 * Increment the count of equivocals
	 * @deprecated	replace with incrementType(Result type)
	 */
	public void incrementEquivocals()
	{
		equivocals++;
	}

	/**
	 * Increment the count of out of domains
	 * @deprecated	replace with incrementType(Result type)
	 */
	public void incrementOutOfDomains()
	{
		outOfDomains++;
	}

	
	/**
	 * @deprecated replaced with getCount(Result type)
	 * @return
	 */
	public int getTruePositives()
	{
		return truePositives;
	}

	/**
	 * @deprecated replaced with getCount(Result type)
	 * @return
	 */
	public int getTrueNegatives()
	{
		return trueNegatives;
	}

	/**
	 * @deprecated replaced with getCount(Result type)
	 * @return
	 */
	public int getFalseNegatives()
	{
		return falseNegatives;
	}

	/**
	 * @deprecated replaced with getCount(Result type)
	 * @return
	 */
	public int getFalsePositives()
	{
		return falsePositives;
	}

	/**
	 * @deprecated replaced with getCount(Result type)
	 * @return
	 */
	public int getEquivocals()
	{
		return equivocals;
	}

	/**
	 * @deprecated replaced with getCount(Result type)
	 * @return
	 */
	public int getOutOfDomians()
	{
		return outOfDomains;
	}
	
}
