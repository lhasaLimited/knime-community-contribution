/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.util;

import java.math.BigDecimal;

/**
 * @author Samuel
 *
 */
public class Binner
{
	
	private int numBins;
	private double interval;
	private double start;
	private double max;
	
	double[] upperBounds;
	double[] lowerBounds;
	
	
	public Binner(int numBins, double interval, double start, double max)
	{
		this.numBins = numBins;
		this.interval = interval;
		this.start = start;
		this.max = max;
		
		upperBounds = calculateBinUpperBounds();
		lowerBounds = calculateLowerBounds(upperBounds, start);
	}
	
	
	/**
     * Gets the string representation of the bin ranges (min value ... max value). The max
     * value is inclusive, and the min value for all but the first bin are exclusive. 
     * 
     * @param numBins			the number of binds
     * @param interval			the interval for the bins
     * @param start				the start position for the bins
     * @param max				the max value for the bins (end point)
     * @return					An array of the bin ranges in ascending order
     */
	public String[] getBinRanges()
	{
		// The first bin is inclusive of everything below it
//		double[] upperBounds = calculateBinUpperBounds(numBins, interval, start, max);
//		double[] lowerBounds = calculateLowerBounds(upperBounds, start);
		
		String[] ranges = new String[numBins];
		
		for(int i = 0; i < upperBounds.length; i++)
		{
			if(i == 0)
				ranges[i] = "[" + lowerBounds[i] + " ... " + upperBounds[i] + "]";
			else
				ranges[i] = "]" + lowerBounds[i] + " ... " + upperBounds[i] + "]";
		}
		
		return ranges;
	}
	
	
	
	/**
	 * Calculate the lower bounds for the ranges
	 * 
	 * @param upperBounds		the calculated upper bounds
	 * @param min				the minimum value (start point)
	 * @return
	 */
	private double[] calculateLowerBounds(double[] upperBounds, double min)
	{
		double[] lowerBounds = new double[upperBounds.length];
		
		lowerBounds[0] = min;
		
		for(int i = 1; i < upperBounds.length; i++)
			lowerBounds[i] = upperBounds[i-1];
		
		return lowerBounds;
	}

	/**
     * Calculates the upper bounds for each bin range. 
     * @param numBins
     * @param interval
     * @return
     */
	private double[] calculateBinUpperBounds()
    {
    	double[] binUpperBounds = new double[numBins];
		
		for(int i = 0; i < numBins; i++)
		{
			Double max = (interval * (i + 1)) + start; // Offset for start position!
			Double truncatedMax = new BigDecimal(max).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
			
			binUpperBounds[i] = truncatedMax;
		}
		
		return binUpperBounds;
    }

   
    /**
     * Identify the bin membership based on the upper bounds. The upper bound is checked for less than
     * or equals to (<=)
     * 
     * @param upperBounds				the upper bounds for the bins
     * @param doubleValue					the value do identify membership for
     * 
     * @return								the index position of the bin
     * 
     * @throws IllegalArgumentException		if the doubleValue exceeds the max bin range an exception is thrown
     */
	public int findBinIndex(double doubleValue) throws IllegalArgumentException
	{
		int failValue = -1;
		int bin = failValue; 
				
		for(int i = 0; (i < upperBounds.length) && (bin == failValue); i++)
		{
			if(doubleValue <= upperBounds[i])
			{
				bin = i;
			} 
			else if(i == upperBounds.length - 1)
			{	
				// If we get to the last bin and we don't match then the value must be > than the max range
				// this should have been checked prior in the configure but we throw this exception just
				// in case. 
				throw new IllegalArgumentException("The value given was greater than the last bin range");	
			}		
		}

		return bin;
	}


	/**
	 * @return
	 */
	public double[] getUpperBounds()
	{
		// TODO Auto-generated method stub
		return upperBounds;
	}
	
	public double[] getLowerBounds()
	{
		return lowerBounds;
	}

}
