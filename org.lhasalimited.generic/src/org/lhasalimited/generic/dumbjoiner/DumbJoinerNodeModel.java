/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.dumbjoiner;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.util.UniqueNameGenerator;

/**
 * This is the model implementation of DumbJoiner.
 * Performs a dumb join. Checks the tables have the same number of rows and then joins row 1 of table 1 to row 1 or table 2, then row 2 or table 1 to row 2 of table 2 etc. 
 *
 * You should probably use the column appender!
 * 
 * @author Lhasa Limited
 */
public class DumbJoinerNodeModel extends NodeModel 
{
    
    /**
     * Constructor for the node model.
     */
    protected DumbJoinerNodeModel() 
    {
        super(2, 1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec) throws Exception 
    {

    	// First let's check the input tables are equal in size
    	if(inData[0].size() != inData[1].size())
    		throw new Exception("The input tables must have an equal number of rows! First table has " + inData[0].size() + " and the second table has " + inData[1].size());
    	
    	
    	long numRows = inData[0].size();
    	
    	BufferedDataContainer container = exec.createDataContainer(createOutputSpec(inData[0].getDataTableSpec(), inData[1].getDataTableSpec()));
    	
    	// TODO: can this be converted to a multi-threaded approach with the column rearranger?
    	// Create a couple of iterators, one for each table.
    	// We will iterate through in sync
    	Iterator<DataRow> rowsOne = inData[0].iterator();
    	Iterator<DataRow> rowsTwo = inData[1].iterator();
    	
    	// Iterate through each row and do the join
    	double track = 1d;
    	while(rowsOne.hasNext() && rowsTwo.hasNext())
    	{
    		exec.setProgress(track / numRows, "Joining rows");
    		container.addRowToTable(createOutputRow(rowsOne.next(), rowsTwo.next()));
    		track++;
    	}
    	
    	// Close the container
    	container.close();
    	
        // Return the container
        return new BufferedDataTable[]{container.getTable()};
    }


   /**
    * Joins the two DataRows into a single dataRow
    * @param Left
    * @param Right
    * @return
    */
	private DataRow createOutputRow(DataRow left, DataRow next2)
	{
		
		DataCell[] cells = new DataCell[left.getNumCells() + next2.getNumCells()];
		
		int i;
		for(i = 0; i < left.getNumCells(); i++)
		{
			cells[i] = left.getCell(i);
		}
		
		for(int j = 0; j < next2.getNumCells(); j++)
		{
			cells[j + i] = next2.getCell(j);
		}
		
		DataRow row = new DefaultRow(left.getKey(), cells);
		
		return row;
	}

	/**
	 * Create the output column specification. This is the first column spec and then the second
     * column spec. 
     * 
	 * @param dataTableSpec		The specification of the first (left) input table
	 * @param dataTableSpec2	The specification of the second (right) intput table
	 * @return
	 */
	private DataTableSpec createOutputSpec(DataTableSpec dataTableSpec, DataTableSpec dataTableSpec2)
	{
		// Join the table specs
//		DataTableSpec outputSpec = new DataTableSpec(dataTableSpec, dataTableSpec2);
		
		DataColumnSpec[] allColSpecs = new DataColumnSpec[dataTableSpec.getNumColumns() + dataTableSpec2.getNumColumns()];

		for(int i = 0; i < dataTableSpec.getNumColumns(); i++)
		{
			allColSpecs[i] = dataTableSpec.getColumnSpec(i);
		}
		
		UniqueNameGenerator namer = new UniqueNameGenerator(dataTableSpec);
		
		// Create the list of current columns to check to see if the 
		// column needs to be renamed
//		List<DataColumnSpec> processed = Arrays.asList(allColSpecs);
		
		for(int j = 0; j < dataTableSpec2.getNumColumns(); j++)
		{		
//			if(processed.contains(dataTableSpec2.getColumnSpec(j)))
//			{
				DataColumnSpec spec = dataTableSpec2.getColumnSpec(j);
//				DataColumnSpecCreator creator = new DataColumnSpecCreator(namer.spec.getName() + "*", spec.getType());
				allColSpecs[j + dataTableSpec.getNumColumns()] = namer.newColumn(spec.getName(), spec.getType());
//			} else
//			{
//				allColSpecs[j + dataTableSpec.getNumColumns()] = dataTableSpec2.getColumnSpec(j);
//			}	
		}
		
		
		
		
		return new DataTableSpec(allColSpecs);
	}

	/**
     * {@inheritDoc}
     */
    @Override
    protected void reset() 
    {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException 
    {
        return new DataTableSpec[]{createOutputSpec(inSpecs[0], inSpecs[1])};
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveSettingsTo(final NodeSettingsWO settings) 
    {
         // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException 
    {
        // TODO: generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException 
    {
        // TODO: generated method stub
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void loadInternals(final File internDir, final ExecutionMonitor exec) throws IOException, CanceledExecutionException 
    {
        // TODO: generated method stub
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    protected void saveInternals(final File internDir, final ExecutionMonitor exec) throws IOException, CanceledExecutionException 
    {
        // TODO: generated method stub
    }

}

