/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.multiperformance;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.RowIterator;
import org.knime.core.data.RowKey;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeLogger;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelFilterString;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.port.PortObjectSpec;
import org.knime.core.node.streamable.InputPortRole;
import org.knime.core.node.streamable.OutputPortRole;
import org.knime.core.node.streamable.PartitionInfo;
import org.knime.core.node.streamable.PortInput;
import org.knime.core.node.streamable.PortOutput;
import org.knime.core.node.streamable.RowInput;
import org.knime.core.node.streamable.RowOutput;
import org.knime.core.node.streamable.StreamableOperator;
import org.knime.core.node.streamable.StreamableOperatorInternals;
import org.lhasalimited.generic.multicolumnrowsplitter.MultiColumnRowSplitterNodeModel;
import org.lhasalimited.generic.util.Result;
import org.lhasalimited.generic.util.Results;

/**
 * This is the model implementation of MultiPerformance. Calculate the
 * performance of multiple models which are provided as seperate columns
 *
 * @author Lhasa Limited
 */
public class MultiPerformanceNodeModel2 extends NodeModel {

	/* start of variables for include exclude */
	/** string contains "selected properties" */
	static final String CONFIG_PREDICTION_COLUMNS = "selected properties";

	/** identifies the included and excluded column selections */
	private final SettingsModelFilterString settingSelectedPredictionColumns = new SettingsModelFilterString(
			CONFIG_PREDICTION_COLUMNS, new ArrayList<String>(), new ArrayList<String>());
	/* end of variables for include exclude */

	// Activity column
	static final String CONFIG_ACTIVITY_COLUMN = "Activity column";
	private final SettingsModelColumnName settingActivityColumn = new SettingsModelColumnName(CONFIG_ACTIVITY_COLUMN,
			"");

	// String entries
	static final String CONFIG_ACTIVE_STRING = "Active flag";
	private final SettingsModelString settingActiveString = new SettingsModelString(CONFIG_ACTIVE_STRING, "active");
	static final String CONFIG_INACTIVE_STRING = "Inactive flag";
	private final SettingsModelString settingInactiveString = new SettingsModelString(CONFIG_INACTIVE_STRING,
			"inactive");
	static final String CONFIG_EQUIVOCAL_STRING = "Equivocal flag";
	private final SettingsModelString settingEquivocalString = new SettingsModelString(CONFIG_EQUIVOCAL_STRING,
			"equivocal");
	static final String CONFIG_OUT_OF_DOMAIN_STRING = "Out of domain flag";
	private final SettingsModelString settingOutOfDomainString = new SettingsModelString(CONFIG_OUT_OF_DOMAIN_STRING,
			"out of domain");
	static final String CONFIG_MISSING_VALUE = "Missing as out of domain";
	private final SettingsModelBoolean settingMissingValueAsOut = new SettingsModelBoolean(CONFIG_MISSING_VALUE, true);

	private static NodeLogger LOGGER = NodeLogger.getLogger(MultiPerformanceNodeModel2.class);

	private final int numOutCols = 16;

	private List<Results> results;

	/**
	 * Constructor for the node model.
	 */
	protected MultiPerformanceNodeModel2() {
		super(1, 2); // Outputs: {The predictions, the rows with issues}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec)
			throws Exception {

		/** data from input port 0 */
		BufferedDataTable input = inData[0];
		/** specification of input data table */
		DataTableSpec inDataSpec = inData[0].getDataTableSpec();

		// The output container for the rows with issues
		// As implemented any issue on any column will put
		// the row to this container. However, that does not
		// mean it didn't successfully get added to a specific
		// set of results.
		BufferedDataContainer containerIssues = exec.createDataContainer(inDataSpec);

		// Configuration
		/** string list of includedColumns containing the prediction values */
		List<String> includedColumns = settingSelectedPredictionColumns.getIncludeList();

		// Get these now to make life easier
		String activityColumnName = settingActivityColumn.getColumnName();
		int activityColumnIndex = inDataSpec.findColumnIndex(activityColumnName);

		/** the index positions of the prediction columns selected */
		int[] predictionColumnPositions = new int[includedColumns.size()];
		for (int i = 0; i < predictionColumnPositions.length; i++) {
			predictionColumnPositions[i] = inDataSpec.findColumnIndex(includedColumns.get(i));
		}

		// Create the map of the predicted values to the enum values
		// This can then be passed into the constructor for the results
		// that need to be created per column
		/** the array of results for the columns selected */
		List<Results> results = new ArrayList<Results>();
		Map<Result, String> stringMap = new TreeMap<Result, String>();
		stringMap.put(Result.EQUIVOCAL, settingEquivocalString.getStringValue());
		stringMap.put(Result.ACTIVE, settingActiveString.getStringValue());
		stringMap.put(Result.INACTIVE, settingInactiveString.getStringValue());
		stringMap.put(Result.OUT_OF_DOMAIN, settingOutOfDomainString.getStringValue());

		// initialise the ArrayList
		for (int i = 0; i < predictionColumnPositions.length; i++) {
			results.add(new Results(stringMap));
		}

		// Iterate through the table calculating the results
		RowIterator rows = input.iterator();
		int rowCounter = 0;
		while (rows.hasNext()) {
			DataRow dataRow = rows.next();
			DataCell cell = dataRow.getCell(activityColumnIndex);

			// Set to true if the row should be added to the issues table
			boolean addToIssuesTable = false;

			if (cell.isMissing()) {
				// If the target activity cell is missing then we
				// can't calculate the performance. This is immediately
				// addedd to the issues table
				LOGGER.warn("Missing value has been ignored");
				addToIssuesTable = true;
			} else if (!targetValueCompatible(((StringCell) cell).getStringValue())) {
				LOGGER.warn("Target value not compatible with specified active and inactive strings");
				addToIssuesTable = true;

			} else {
				// For each prediction increment the result
				String activity = ((StringCell) cell).getStringValue();

				for (int i = 0; i < predictionColumnPositions.length; i++) {
					Results currentResult = results.get(i);
					DataCell predictionCell = dataRow.getCell(predictionColumnPositions[i]);

					// If the prediction cell is not missing call the increment
					// method with
					// the string values. Otherwise handle according to missing
					// value selection
					if (!predictionCell.isMissing()) {
						String prediction = ((StringCell) predictionCell).getStringValue();
						currentResult.increment(activity, prediction);
					} else {
						// If we want to treat missing values as out of domain
						// pass in the prediction as a null value and don't add
						// to the issues table. Otherwise don't increment the
						// results
						// and add to the issues table
						if (settingMissingValueAsOut.getBooleanValue()) {
							currentResult.increment(activity, null);
						} else {
							addToIssuesTable = true;
						}
					}
				}
			}

			// Add the row if it was missing
			if (addToIssuesTable)
				containerIssues.addRowToTable(dataRow);

			rowCounter++;
			exec.setProgress((double) rowCounter / input.size());
			exec.checkCanceled();
		}

		// Output results
		DataTableSpec outputSpec = getOutputDataTableSpec();
		BufferedDataContainer container = exec.createDataContainer(outputSpec);

		for (int i = 0; i < predictionColumnPositions.length; i++) {
			container.addRowToTable(createRow(new RowKey(includedColumns.get(i)), results.get(i)));
		}

		// Close containers
		container.close();
		containerIssues.close();

		return new BufferedDataTable[] { container.getTable(), containerIssues.getTable() };

	}

	private boolean targetValueCompatible(String stringValue) 
	{
		boolean compatible = false;
		
		if(stringValue.equals(settingActiveString.getStringValue()))
			compatible = true;
		
		if(stringValue.equals(settingInactiveString.getStringValue()))
			compatible = true;
		
		return compatible;
	}

	/**
	 * Create a new row given the results and the RowKey
	 * 
	 * @param rowKey
	 *            The RowKey, this should be based on the column header
	 * @param results
	 *            The results for the given column
	 * 
	 * @return
	 */
	private DataRow createRow(RowKey rowKey, Results results) {
		DataCell[] cells = new DataCell[numOutCols];

		cells[0] = new DoubleCell(results.getBalancedAccuracy());
		cells[1] = new DoubleCell(results.getAccuracy());
		cells[2] = new DoubleCell(results.getSensitivity());
		cells[3] = new DoubleCell(results.getSpecificity());
		cells[4] = new DoubleCell(results.getPrecision());
		cells[5] = new DoubleCell(results.getNegativePredictivity());
		cells[6] = new DoubleCell(results.getRecall());
		cells[7] = new DoubleCell(results.getFMeasure());
		cells[8] = new DoubleCell(results.calculateMCC());
		cells[9] = new IntCell(results.getTotal());
		cells[10] = new IntCell(results.getCount(Result.TRUE_POSITIVE));
		cells[11] = new IntCell(results.getCount(Result.FALSE_POSITIVE));
		cells[12] = new IntCell(results.getCount(Result.TRUE_NEGATIVE));
		cells[13] = new IntCell(results.getCount(Result.FALSE_NEGATIVE));
		cells[14] = new IntCell(results.getCount(Result.EQUIVOCAL));
		cells[15] = new IntCell(results.getCount(Result.OUT_OF_DOMAIN));

		return new DefaultRow(rowKey, cells);
	}

	/**
	 * Create an array of {@link DataCell} representing the various measures
	 * that can be returned from the Results object.
	 * 
	 * @param results
	 *            The results to create the table cells from
	 * 
	 * @return The array of cells to then add to a row
	 * @deprecated
	 */
	private DataCell[] createCells(Results current) {

		DataCell[] cells = new DataCell[numOutCols];

		cells[0] = new DoubleCell(current.getBalancedAccuracy());
		cells[1] = new DoubleCell(current.getAccuracy());
		cells[2] = new DoubleCell(current.getSensitivity());
		cells[3] = new DoubleCell(current.getSpecificity());
		cells[4] = new DoubleCell(current.getPrecision());
		cells[5] = new DoubleCell(current.getNegativePredictivity());
		cells[6] = new DoubleCell(current.getRecall());
		cells[7] = new DoubleCell(current.getFMeasure());
		cells[8] = new IntCell(current.getTotal());
		cells[9] = new IntCell(current.getCount(Result.TRUE_POSITIVE));
		cells[10] = new IntCell(current.getCount(Result.FALSE_POSITIVE));
		cells[11] = new IntCell(current.getCount(Result.TRUE_NEGATIVE));
		cells[12] = new IntCell(current.getCount(Result.FALSE_NEGATIVE));
		cells[13] = new IntCell(current.getCount(Result.EQUIVOCAL));
		cells[14] = new IntCell(current.getCount(Result.OUT_OF_DOMAIN));

		return cells;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset() {
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException {

		// TODO: generated method stub
		return new DataTableSpec[] { getOutputDataTableSpec(), inSpecs[0] };
	}

	/**
	 * Create the output specification based on the measures available from
	 * {@link Results}
	 * 
	 * @return
	 */
	private DataTableSpec getOutputDataTableSpec() {

		DataColumnSpec[] outColSpec = new DataColumnSpec[numOutCols];

		outColSpec[0] = new DataColumnSpecCreator("Balanced Accuracy", DoubleCell.TYPE).createSpec();
		outColSpec[1] = new DataColumnSpecCreator("Accuracy", DoubleCell.TYPE).createSpec();
		outColSpec[2] = new DataColumnSpecCreator("Sensitivity", DoubleCell.TYPE).createSpec();
		outColSpec[3] = new DataColumnSpecCreator("Specificity", DoubleCell.TYPE).createSpec();
		outColSpec[4] = new DataColumnSpecCreator("PPV", DoubleCell.TYPE).createSpec();
		outColSpec[5] = new DataColumnSpecCreator("NPV", DoubleCell.TYPE).createSpec();
		outColSpec[6] = new DataColumnSpecCreator("Recall", DoubleCell.TYPE).createSpec();
		outColSpec[7] = new DataColumnSpecCreator("F-Measure", DoubleCell.TYPE).createSpec();
		outColSpec[8] = new DataColumnSpecCreator("MCC", DoubleCell.TYPE).createSpec();
		outColSpec[9] = new DataColumnSpecCreator("Total", IntCell.TYPE).createSpec();
		outColSpec[10] = new DataColumnSpecCreator("TP", IntCell.TYPE).createSpec();
		outColSpec[11] = new DataColumnSpecCreator("FP", IntCell.TYPE).createSpec();
		outColSpec[12] = new DataColumnSpecCreator("TN", IntCell.TYPE).createSpec();
		outColSpec[13] = new DataColumnSpecCreator("FN", IntCell.TYPE).createSpec();
		outColSpec[14] = new DataColumnSpecCreator("Equivocal", IntCell.TYPE).createSpec();
		outColSpec[15] = new DataColumnSpecCreator("Out of Domain", IntCell.TYPE).createSpec();

		return new DataTableSpec(outColSpec);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings) {
		settingSelectedPredictionColumns.saveSettingsTo(settings);
		settingActiveString.saveSettingsTo(settings);
		settingInactiveString.saveSettingsTo(settings);
		settingEquivocalString.saveSettingsTo(settings);
		settingOutOfDomainString.saveSettingsTo(settings);
		settingActivityColumn.saveSettingsTo(settings);
		settingMissingValueAsOut.saveSettingsTo(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException {
		settingSelectedPredictionColumns.loadSettingsFrom(settings);
		settingActiveString.loadSettingsFrom(settings);
		settingInactiveString.loadSettingsFrom(settings);
		settingEquivocalString.loadSettingsFrom(settings);
		settingOutOfDomainString.loadSettingsFrom(settings);
		settingActivityColumn.loadSettingsFrom(settings);
		settingMissingValueAsOut.loadSettingsFrom(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException {
		settingSelectedPredictionColumns.validateSettings(settings);
		settingActiveString.validateSettings(settings);
		settingInactiveString.validateSettings(settings);
		settingEquivocalString.validateSettings(settings);
		settingOutOfDomainString.validateSettings(settings);
		settingActivityColumn.validateSettings(settings);
		settingMissingValueAsOut.validateSettings(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException {
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException {
		// TODO: generated method stub
	}

	///////////////////////////
	///// Streaming API

	/** {@inheritDoc} */
	@Override
	public InputPortRole[] getInputPortRoles() {
		return new InputPortRole[] { InputPortRole.NONDISTRIBUTED_STREAMABLE };
	}

	/** {@inheritDoc} */
	@Override
	public OutputPortRole[] getOutputPortRoles() {
		return new OutputPortRole[] { OutputPortRole.NONDISTRIBUTED, OutputPortRole.NONDISTRIBUTED };
	}

	/** {@inheritDoc} */
	@Override
	public StreamableOperator createStreamableOperator(final PartitionInfo partitionInfo,
			final PortObjectSpec[] inSpecs) throws InvalidSettingsException {
		return new StreamableOperator() {

			@Override
			public StreamableOperatorInternals saveInternals() {
				return null;
			}

			@Override
			public void runFinal(final PortInput[] inputs, final PortOutput[] outputs, final ExecutionContext ctx)
					throws Exception {
				RowInput in = (RowInput) inputs[0];
				// Create the list to buffer the results
				results = createResultsList(in.getDataTableSpec());
				RowOutput out = (RowOutput) outputs[0];
				RowOutput out1 = (RowOutput) outputs[1];
				MultiPerformanceNodeModel2.this.execute(in, out1, ctx);

				// Now that all the results have been collected we can calculate
				// the performance of the model
				// and then push the results as each model been calculated
				for (int i = 0; i < getPredictionColumnPositions(in.getDataTableSpec()).length; i++)
					out.push(createRow(new RowKey(settingSelectedPredictionColumns.getIncludeList().get(i)),
							results.get(i)));

				out.close();
				// Clear this from memory?
				results.clear();
			}
		};
	}

	protected void execute(RowInput inData, RowOutput outData, ExecutionContext ctx) throws InterruptedException {
		DataRow row;
		while ((row = inData.poll()) != null) {
			String activityColumnName = settingActivityColumn.getColumnName();
			int activityColumnIndex = inData.getDataTableSpec().findColumnIndex(activityColumnName);

			DataCell cell = row.getCell(activityColumnIndex);

			int[] predictionColumnPositions = getPredictionColumnPositions(inData.getDataTableSpec());

			// Set to true if the row should be added to the issues table
			boolean addToIssuesTable = false;

			if (cell.isMissing()) {
				// If the target activity cell is missing then we
				// can't calculate the performance. This is immediately
				// addedd to the issues table
				LOGGER.warn("Missing value has been ignored");
				addToIssuesTable = true;
			} else {
				// For each prediction increment the result
				String activity = ((StringCell) cell).getStringValue();

				for (int i = 0; i < predictionColumnPositions.length; i++) {
					Results currentResult = results.get(i);
					DataCell predictionCell = row.getCell(predictionColumnPositions[i]);

					// If the prediction cell is not missing call the increment
					// method with
					// the string values. Otherwise handle according to missing
					// value selection
					if (!predictionCell.isMissing()) {
						String prediction = ((StringCell) predictionCell).getStringValue();
						currentResult.increment(activity, prediction);
					} else {
						// If we want to treat missing values as out of domain
						// pass in the prediction as a null value and don't add
						// to the issues table. Otherwise don't increment the
						// results
						// and add to the issues table
						if (settingMissingValueAsOut.getBooleanValue()) {
							currentResult.increment(activity, null);
						} else {
							addToIssuesTable = true;
						}
					}
				}

				if (addToIssuesTable) {
					// We push the error out immediately
					outData.push(row);
				}
			}

		}
		inData.close();
		outData.close();
	}

	private int[] getPredictionColumnPositions(DataTableSpec inDataSpec) {
		/** the index positions of the prediction columns selected */
		int[] predictionColumnPositions = new int[settingSelectedPredictionColumns.getIncludeList().size()];
		for (int i = 0; i < predictionColumnPositions.length; i++) {
			predictionColumnPositions[i] = inDataSpec
					.findColumnIndex(settingSelectedPredictionColumns.getIncludeList().get(i));
		}

		return predictionColumnPositions;
	}

	private List<Results> createResultsList(DataTableSpec inDataSpec) {

		/** the index positions of the prediction columns selected */
		int[] predictionColumnPositions = getPredictionColumnPositions(inDataSpec);

		// Create the map of the predicted values to the enum values
		// This can then be passed into the constructor for the results
		// that need to be created per column
		/** the array of results for the columns selected */
		List<Results> results = new ArrayList<Results>();
		Map<Result, String> stringMap = new TreeMap<Result, String>();
		stringMap.put(Result.EQUIVOCAL, settingEquivocalString.getStringValue());
		stringMap.put(Result.ACTIVE, settingActiveString.getStringValue());
		stringMap.put(Result.INACTIVE, settingInactiveString.getStringValue());
		stringMap.put(Result.OUT_OF_DOMAIN, settingOutOfDomainString.getStringValue());

		// initialise the ArrayList
		for (int i = 0; i < predictionColumnPositions.length; i++) {
			results.add(new Results(stringMap));
		}

		return results;
	}

}
