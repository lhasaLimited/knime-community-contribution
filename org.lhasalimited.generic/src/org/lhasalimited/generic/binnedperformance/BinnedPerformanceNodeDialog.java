/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.binnedperformance;

import org.knime.core.data.DoubleValue;
import org.knime.core.data.IntValue;
import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;
import org.knime.core.node.defaultnodesettings.DialogComponentBoolean;
import org.knime.core.node.defaultnodesettings.DialogComponentColumnNameSelection;
import org.knime.core.node.defaultnodesettings.DialogComponentDoubleRange;
import org.knime.core.node.defaultnodesettings.DialogComponentNumber;
import org.knime.core.node.defaultnodesettings.DialogComponentString;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelDoubleRange;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;

/**
 * <code>NodeDialog</code> for the "BinnedPerformance" Node.
 * Bins the values based on a specific column and then calculates the performance for each bin
 *
 * This node dialog derives from {@link DefaultNodeSettingsPane} which allows
 * creation of a simple dialog with standard components. If you need a more 
 * complex dialog please derive directly from 
 * {@link org.knime.core.node.NodeDialogPane}.
 * 
 * @author Lhasa Limited
 */
public class BinnedPerformanceNodeDialog extends DefaultNodeSettingsPane 
{


    protected BinnedPerformanceNodeDialog() 
    {
    	// Column selection
    	createNewGroup("Column setup");
    	setHorizontalPlacement(true);
		
		addDialogComponent(new DialogComponentColumnNameSelection(new SettingsModelColumnName(BinnedPerformanceNodeModel.CONFIG_TARGET_COLUMN, "Target"), "Target:", 0, StringValue.class));
		addDialogComponent(new DialogComponentColumnNameSelection(new SettingsModelColumnName(BinnedPerformanceNodeModel.CONFIG_PREDICTION_COLUMN, "Prediction"), "Prediction:", 0, StringValue.class));
		addDialogComponent(new DialogComponentColumnNameSelection(new SettingsModelColumnName(BinnedPerformanceNodeModel.CONFIG_BINNING_COLUMN, "Probability"), "Probability:", 0, DoubleValue.class, IntValue.class));
		
		// Result configuration
		createNewGroup("Prediction setup");
		
		addDialogComponent(new DialogComponentString(new SettingsModelString(BinnedPerformanceNodeModel.CONFIG_POSITIVE_STRING, "active"), "Positive string:"));
		addDialogComponent(new DialogComponentString(new SettingsModelString(BinnedPerformanceNodeModel.CONFIG_NEGATIVE_STRING, "inactive"), "Negative string:"));
		setHorizontalPlacement(false);
		setHorizontalPlacement(true);
		addDialogComponent(new DialogComponentString(new SettingsModelString(BinnedPerformanceNodeModel.CONFIG_EQUIVOCAL_STRING, "equivocal"), "Equivocal string:"));
		addDialogComponent(new DialogComponentString(new SettingsModelString(BinnedPerformanceNodeModel.CONFIG_OUT_OF_DOMAIN_STRING, "out of domain"), "Out of domain string:"));
		setHorizontalPlacement(false);
		addDialogComponent(new DialogComponentBoolean(new SettingsModelBoolean(BinnedPerformanceNodeModel.CONFIG_MISSING_VALUE, true), "Missing as out of domain"));
		
		createNewGroup("Range");
		addDialogComponent(new DialogComponentNumber(new SettingsModelIntegerBounded(BinnedPerformanceNodeModel.CONFIG_NUM_BINS, 5, 0, Integer.MAX_VALUE), "Number of bins:", 1));
		addDialogComponent(new DialogComponentDoubleRange(new SettingsModelDoubleRange(BinnedPerformanceNodeModel.CONFIG_RANGE, 0.5d, 1d), Double.MAX_VALUE * - 1, Double.MAX_VALUE, 0.1, "Range"));
    
		createNewGroup("Fail option");
		addDialogComponent(new DialogComponentBoolean(new SettingsModelBoolean(BinnedPerformanceNodeModel.CONFIG_FAIL_HARD, true), "Fail on range issues"));
    
    }
}

