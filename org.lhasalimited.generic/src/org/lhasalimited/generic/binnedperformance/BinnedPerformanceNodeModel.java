/**
 * Copyright (C) 2015  Lhasa Limited
 * 
 * These KNIME nodes are free software: you can redistribute it and/or modify it under the terms of the GNU General 
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) 
 * any later version.
 * 
 * These KNIME nodes are distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the 
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with this KNIME node.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * These nodes extend the KNIME core which is free software developed by KNIME Gmbh (http://www.knime.org/).
 * 
 * These nodes are made available without charge and is not intended for consumer use.  It is your responsibility 
 * to check that it is suitable for your needs, to incorporate it into your installation of KNIME and to check any 
 * results obtained by use of the node. 
 * 
 * Lhasa does not provide support services or training in relation to the KNIME nodes.
 * 
 */
package org.lhasalimited.generic.binnedperformance;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnDomain;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.DataTableSpecCreator;
import org.knime.core.data.DoubleValue;
import org.knime.core.data.RowIterator;
import org.knime.core.data.RowKey;
import org.knime.core.data.StringValue;
import org.knime.core.data.append.AppendedColumnRow;
import org.knime.core.data.def.DefaultRow;
import org.knime.core.data.def.DoubleCell;
import org.knime.core.data.def.IntCell;
import org.knime.core.data.def.StringCell;
import org.knime.core.data.def.StringCell.StringCellFactory;
import org.knime.core.node.BufferedDataContainer;
import org.knime.core.node.BufferedDataTable;
import org.knime.core.node.CanceledExecutionException;
import org.knime.core.node.ExecutionContext;
import org.knime.core.node.ExecutionMonitor;
import org.knime.core.node.InvalidSettingsException;
import org.knime.core.node.NodeModel;
import org.knime.core.node.NodeSettingsRO;
import org.knime.core.node.NodeSettingsWO;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelDoubleRange;
import org.knime.core.node.defaultnodesettings.SettingsModelIntegerBounded;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.knime.core.node.port.PortObjectSpec;
import org.knime.core.node.streamable.InputPortRole;
import org.knime.core.node.streamable.OutputPortRole;
import org.knime.core.node.streamable.PartitionInfo;
import org.knime.core.node.streamable.PortInput;
import org.knime.core.node.streamable.PortOutput;
import org.knime.core.node.streamable.RowInput;
import org.knime.core.node.streamable.RowOutput;
import org.knime.core.node.streamable.StreamableOperator;
import org.knime.core.node.streamable.StreamableOperatorInternals;
import org.lhasalimited.generic.util.Binner;
import org.lhasalimited.generic.util.Result;
import org.lhasalimited.generic.util.Results;

/**
 * This is the model implementation of BinnedPerformance. Bins the values based
 * on a specific column and then calculates the performance for each bin
 *
 * @author Lhasa Limited
 */
public class BinnedPerformanceNodeModel extends NodeModel
{

	static final String CONFIG_TARGET_COLUMN = "Activity column";
	private final SettingsModelColumnName settingTargetColumn = new SettingsModelColumnName(CONFIG_TARGET_COLUMN,
			"Target");

	static final String CONFIG_BINNING_COLUMN = "Binning column";
	private final SettingsModelColumnName settingBinningColumn = new SettingsModelColumnName(CONFIG_BINNING_COLUMN,
			"Probability");

	static final String CONFIG_PREDICTION_COLUMN = "Prediction column";
	private final SettingsModelColumnName settingPredictionColumn = new SettingsModelColumnName(
			CONFIG_PREDICTION_COLUMN, "Prediction");

	// String entries
	static final String CONFIG_POSITIVE_STRING = "Active flag";
	private final SettingsModelString settingActiveString = new SettingsModelString(CONFIG_POSITIVE_STRING, "active");
	static final String CONFIG_NEGATIVE_STRING = "Inactive flag";
	private final SettingsModelString settingInactiveString = new SettingsModelString(CONFIG_NEGATIVE_STRING,
			"inactive");
	static final String CONFIG_EQUIVOCAL_STRING = "Equivocal flag";
	private final SettingsModelString settingEquivocalString = new SettingsModelString(CONFIG_EQUIVOCAL_STRING,
			"equivocal");
	static final String CONFIG_OUT_OF_DOMAIN_STRING = "Out of domain flag";
	private final SettingsModelString settingOutOfDomainString = new SettingsModelString(CONFIG_OUT_OF_DOMAIN_STRING,
			"out of domain");
	static final String CONFIG_MISSING_VALUE = "Missing as out of domain";
	private final SettingsModelBoolean settingMissingValueAsOut = new SettingsModelBoolean(CONFIG_MISSING_VALUE, true);

	static final String CONFIG_RANGE = "Min range";
	private final SettingsModelDoubleRange settingRange = new SettingsModelDoubleRange(CONFIG_RANGE, 0.5d, 1d);
	static final String CONFIG_NUM_BINS = "Number of bins";
	private final SettingsModelIntegerBounded settingNumberOfbins = new SettingsModelIntegerBounded(CONFIG_NUM_BINS, 5,
			0, Integer.MAX_VALUE);

	static final String CONFIG_FAIL_HARD = "Fail hard";
	private final SettingsModelBoolean settingFailhard = new SettingsModelBoolean(CONFIG_FAIL_HARD, true);

	private int numOutCols = 15;

	private List<Results> streamResults;

	private int targetColIndex;
	private int predictionColIndex;
	private int valueColIndex;

	protected BinnedPerformanceNodeModel()
	{
		super(1, 2);
	}

	/**
	 * {@inheritDoc} </br>
	 * </br>
	 * Outputs two tables: the first with the calculated results for the ranges
	 * the second the failed rows. A row may fail because it has a missing value
	 * in the binning column or the value falls outside the specified range. How
	 * the range issue is handled depends on the configuration of the fail hard
	 * setting
	 */
	@Override
	protected BufferedDataTable[] execute(final BufferedDataTable[] inData, final ExecutionContext exec)
			throws Exception
	{
		// First need to calculate the ranges
		double min = settingRange.getMinRange();
		double max = settingRange.getMaxRange();
		int numBins = settingNumberOfbins.getIntValue();

		// Calculate the bin interval
		double interval = (max - min) / numBins;

		List<Results> results = createResultsList(numBins);

		/** data from input port 0 */
		BufferedDataTable input = inData[0];
		/** specification of input data table */
		DataTableSpec inDataSpec = inData[0].getDataTableSpec();

		Binner binner = new Binner(numBins, interval, min, max);

		// double[] upperBounds = calculateBinUpperBounds(numBins, interval,
		// min, max);
		String[] binRanges = binner.getBinRanges();

		// Create the
		BufferedDataContainer failedRows = exec.createDataContainer(createErrorSpec(inDataSpec));

		// Iterate through the table calculating the results
		RowIterator rows = input.iterator();
		int rowCounter = 0;
		int failedRowCount = 0;
		while (rows.hasNext())
		{
			DataRow dataRow = rows.next();

			// Get the cells
			DataCell targetCell = dataRow.getCell(inDataSpec.findColumnIndex(settingTargetColumn.getColumnName()));
			DataCell predictionCell = dataRow
					.getCell(inDataSpec.findColumnIndex(settingPredictionColumn.getColumnName()));
			DataCell valueCell = dataRow.getCell(inDataSpec.findColumnIndex(settingBinningColumn.getColumnName()));

			List<String> errors = new ArrayList<>();
			
			if (targetCell.isMissing() || valueCell.isMissing())
			{
				failedRowCount++; // We fail as we can't compute the bin if the
									// value is missing or the activity if the
									// target is missing
				errors.add("Target or prediction value missing");
				
			} else if (!targetValueCompatible(((StringCell) targetCell).getStringValue())) 
			{
				getLogger().warn("Target value not compatible with specified active and inactive strings");
				failedRowCount++;
				errors.add("Target value not compatible with specified active and inactive strings");
			}
			else
			{
				try
				{
					double value = ((DoubleValue) valueCell).getDoubleValue();

					// Check to see if the value is smaller than the configured
					// min
					if (value < min)
						throw new IllegalArgumentException("Value falls below specified range");

					// Find the bin index
					int binIndex = binner.findBinIndex(value);

					if (predictionCell.isMissing())
					{
						if (settingMissingValueAsOut.getBooleanValue())
						{
							results.get(binIndex).increment(((StringValue) targetCell).getStringValue(), null);
						} else
						{
							failedRowCount++;
							errors.add("Target value is missing");
						}
					} else
					{
						results.get(binIndex).increment(((StringValue) targetCell).getStringValue(),
								((StringValue) predictionCell).getStringValue());
					}
				} catch (Exception e)
				{
					errors.add(e.getMessage());
				}
			}
			
			if(!errors.isEmpty())
				failedRows.addRowToTable(createErrorRow(dataRow, errors));

			rowCounter++;
			exec.setProgress((double) rowCounter / input.size());

		}

		// Output results
		DataTableSpec outputSpec = getOutputDataTableSpec();
		BufferedDataContainer container = exec.createDataContainer(outputSpec);

		for (int i = 0; i < results.size(); i++)
		{
			container.addRowToTable(createRow(new RowKey(binRanges[i]), results.get(i)));
		}

		if (failedRowCount > 0)
			setWarningMessage(failedRowCount + " rows failed");

		container.close();
		failedRows.close();

		return new BufferedDataTable[] { container.getTable(), failedRows.getTable() };
	}

	private boolean targetValueCompatible(String stringValue) 
	{
		boolean compatible = false;
		
		if(stringValue.equals(settingActiveString.getStringValue()))
			compatible = true;
		
		if(stringValue.equals(settingInactiveString.getStringValue()))
			compatible = true;
		
		return compatible;
	}

	
	private DataRow createErrorRow(DataRow dataRow, List<String> errors)
	{
		return new AppendedColumnRow(dataRow, StringCellFactory.create(errors.toString()));
	}

	private List<Results> createResultsList(int numBins)
	{
		// Create the map of the predicted values to the enum values
		// This can then be passed into the constructor for the results
		// that need to be created per column
		/** the array of results for the columns selected */
		List<Results> results = new ArrayList<Results>();
		Map<Result, String> stringMap = new TreeMap<Result, String>();
		stringMap.put(Result.EQUIVOCAL, settingEquivocalString.getStringValue());
		stringMap.put(Result.ACTIVE, settingActiveString.getStringValue());
		stringMap.put(Result.INACTIVE, settingInactiveString.getStringValue());
		stringMap.put(Result.OUT_OF_DOMAIN, settingOutOfDomainString.getStringValue());

		for (int i = 0; i < numBins; i++)
		{
			results.add(new Results(stringMap));
		}

		return results;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void reset()
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected DataTableSpec[] configure(final DataTableSpec[] inSpecs) throws InvalidSettingsException
	{
		if (settingFailhard.getBooleanValue())
		{
			DataColumnDomain domain = inSpecs[0].getColumnSpec(settingBinningColumn.getColumnName()).getDomain();

			if (domain.hasBounds())
			{
				double lower = ((DoubleCell) domain.getLowerBound()).getDoubleValue();
				double upper = ((DoubleCell) domain.getUpperBound()).getDoubleValue();

				if (lower < settingRange.getMinRange() || upper > settingRange.getMaxRange())
					throw new InvalidSettingsException("Range specified (" + settingRange.getMinRange() + " - "
							+ settingRange.getMaxRange() + ") does not conform with column domain");
			}

			double min = settingRange.getMinRange();
			double max = settingRange.getMaxRange();
			int numBins = settingNumberOfbins.getIntValue();

			double interval = (max - min) / numBins;

			Binner binner = new Binner(numBins, interval, min, max);

			double[] upperBounds = binner.getUpperBounds();

			if (upperBounds[upperBounds.length - 1] < max)
				throw new InvalidSettingsException("The last calculated upper bound is lower than the max value");
		}

		return new DataTableSpec[] { getOutputDataTableSpec(), createErrorSpec(inSpecs[0]) };
	}
	
	private DataTableSpec createErrorSpec(DataTableSpec inSpec)
	{
		DataTableSpecCreator creator = new DataTableSpecCreator(inSpec);
		creator.addColumns(new DataColumnSpecCreator(DataTableSpec.getUniqueColumnName(inSpec, "Error cause"), StringCellFactory.TYPE).createSpec());
		
		return creator.createSpec();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveSettingsTo(final NodeSettingsWO settings)
	{
		settingTargetColumn.saveSettingsTo(settings);
		settingPredictionColumn.saveSettingsTo(settings);
		settingBinningColumn.saveSettingsTo(settings);
		settingActiveString.saveSettingsTo(settings);
		settingInactiveString.saveSettingsTo(settings);
		settingEquivocalString.saveSettingsTo(settings);
		settingOutOfDomainString.saveSettingsTo(settings);
		settingTargetColumn.saveSettingsTo(settings);
		settingMissingValueAsOut.saveSettingsTo(settings);
		settingRange.saveSettingsTo(settings);
		settingNumberOfbins.saveSettingsTo(settings);
		settingFailhard.saveSettingsTo(settings);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadValidatedSettingsFrom(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		settingTargetColumn.loadSettingsFrom(settings);
		settingPredictionColumn.loadSettingsFrom(settings);
		;
		settingBinningColumn.loadSettingsFrom(settings);
		settingActiveString.loadSettingsFrom(settings);
		settingInactiveString.loadSettingsFrom(settings);
		settingEquivocalString.loadSettingsFrom(settings);
		settingOutOfDomainString.loadSettingsFrom(settings);
		settingTargetColumn.loadSettingsFrom(settings);
		settingMissingValueAsOut.loadSettingsFrom(settings);
		settingRange.loadSettingsFrom(settings);
		settingNumberOfbins.loadSettingsFrom(settings);
		settingFailhard.loadSettingsFrom(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void validateSettings(final NodeSettingsRO settings) throws InvalidSettingsException
	{
		settingTargetColumn.validateSettings(settings);
		settingPredictionColumn.validateSettings(settings);
		settingBinningColumn.validateSettings(settings);
		settingActiveString.validateSettings(settings);
		settingInactiveString.validateSettings(settings);
		settingEquivocalString.validateSettings(settings);
		settingOutOfDomainString.validateSettings(settings);
		settingTargetColumn.validateSettings(settings);
		settingMissingValueAsOut.validateSettings(settings);
		settingRange.validateSettings(settings);
		settingNumberOfbins.validateSettings(settings);
		settingFailhard.validateSettings(settings);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void loadInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveInternals(final File internDir, final ExecutionMonitor exec)
			throws IOException, CanceledExecutionException
	{
		// TODO: generated method stub
	}

	/**
	 * Create the output specification based on the measures available from
	 * {@link Results}
	 * 
	 * @return
	 */
	private DataTableSpec getOutputDataTableSpec()
	{

		DataColumnSpec[] outColSpec = new DataColumnSpec[numOutCols];

		outColSpec[0] = new DataColumnSpecCreator("Balanced Accuracy", DoubleCell.TYPE).createSpec();
		outColSpec[1] = new DataColumnSpecCreator("Accuracy", DoubleCell.TYPE).createSpec();
		outColSpec[2] = new DataColumnSpecCreator("Sensitivity", DoubleCell.TYPE).createSpec();
		outColSpec[3] = new DataColumnSpecCreator("Specificity", DoubleCell.TYPE).createSpec();
		outColSpec[4] = new DataColumnSpecCreator("PPV", DoubleCell.TYPE).createSpec();
		outColSpec[5] = new DataColumnSpecCreator("NPV", DoubleCell.TYPE).createSpec();
		outColSpec[6] = new DataColumnSpecCreator("Recall", DoubleCell.TYPE).createSpec();
		outColSpec[7] = new DataColumnSpecCreator("F-Measure", DoubleCell.TYPE).createSpec();
		outColSpec[8] = new DataColumnSpecCreator("Total", IntCell.TYPE).createSpec();
		outColSpec[9] = new DataColumnSpecCreator("TP", IntCell.TYPE).createSpec();
		outColSpec[10] = new DataColumnSpecCreator("FP", IntCell.TYPE).createSpec();
		outColSpec[11] = new DataColumnSpecCreator("TN", IntCell.TYPE).createSpec();
		outColSpec[12] = new DataColumnSpecCreator("FN", IntCell.TYPE).createSpec();
		outColSpec[13] = new DataColumnSpecCreator("Equivocal", IntCell.TYPE).createSpec();
		outColSpec[14] = new DataColumnSpecCreator("Out of Domain", IntCell.TYPE).createSpec();

		return new DataTableSpec(outColSpec);

	}

	/**
	 * Create a new row given the results and the RowKey
	 * 
	 * @param rowKey
	 *            The RowKey, this should be based on the bin range
	 * @param results
	 *            The results for the given column
	 * 
	 * @return
	 */
	private DataRow createRow(RowKey rowKey, Results results)
	{
		DataCell[] cells = new DataCell[numOutCols];

		cells[0] = new DoubleCell(results.getBalancedAccuracy());
		cells[1] = new DoubleCell(results.getAccuracy());
		cells[2] = new DoubleCell(results.getSensitivity());
		cells[3] = new DoubleCell(results.getSpecificity());
		cells[4] = new DoubleCell(results.getPrecision());
		cells[5] = new DoubleCell(results.getNegativePredictivity());
		cells[6] = new DoubleCell(results.getRecall());
		cells[7] = new DoubleCell(results.getFMeasure());
		cells[8] = new IntCell(results.getTotal());
		cells[9] = new IntCell(results.getCount(Result.TRUE_POSITIVE));
		cells[10] = new IntCell(results.getCount(Result.FALSE_POSITIVE));
		cells[11] = new IntCell(results.getCount(Result.TRUE_NEGATIVE));
		cells[12] = new IntCell(results.getCount(Result.FALSE_NEGATIVE));
		cells[13] = new IntCell(results.getCount(Result.EQUIVOCAL));
		cells[14] = new IntCell(results.getCount(Result.OUT_OF_DOMAIN));

		return new DefaultRow(rowKey, cells);
	}

	///////////////////////////////
	////// Strreaming API

	public InputPortRole[] getInputPortRoles()
	{
		return new InputPortRole[] { InputPortRole.NONDISTRIBUTED_STREAMABLE };
	}

	/** {@inheritDoc} */
	@Override
	public OutputPortRole[] getOutputPortRoles()
	{
		return new OutputPortRole[] { OutputPortRole.NONDISTRIBUTED, OutputPortRole.NONDISTRIBUTED };
	}

	/** {@inheritDoc} */
	@Override
	public StreamableOperator createStreamableOperator(final PartitionInfo partitionInfo,
			final PortObjectSpec[] inSpecs) throws InvalidSettingsException
	{
		return new StreamableOperator()
		{

			@Override
			public StreamableOperatorInternals saveInternals()
			{
				return null;
			}

			@Override
			public void runFinal(final PortInput[] inputs, final PortOutput[] outputs, final ExecutionContext ctx)
					throws Exception
			{
				RowInput in = (RowInput) inputs[0];
				// Create the list to buffer the results
				streamResults = createResultsList(settingNumberOfbins.getIntValue());
				RowOutput out = (RowOutput) outputs[0];
				RowOutput out1 = (RowOutput) outputs[1];

				// First need to calculate the ranges
				double min = settingRange.getMinRange();
				double max = settingRange.getMaxRange();
				int numBins = settingNumberOfbins.getIntValue();
				double interval = (max - min) / numBins;

				Binner binner = new Binner(numBins, interval, min, max);
				String[] binRanges = binner.getBinRanges();

				// Get the cells
				targetColIndex = in.getDataTableSpec().findColumnIndex(settingTargetColumn.getColumnName());
				predictionColIndex = in.getDataTableSpec().findColumnIndex(settingPredictionColumn.getColumnName());
				valueColIndex = in.getDataTableSpec().findColumnIndex(settingBinningColumn.getColumnName());

				BinnedPerformanceNodeModel.this.execute(in, out1, ctx, binner, min, max);

				for (int i = 0; i < streamResults.size(); i++)
					out.push(createRow(new RowKey(binRanges[i]), streamResults.get(i)));

				out.close();
			}
		};
	}

	protected void execute(RowInput inData, RowOutput outError, ExecutionContext ctx, Binner binner, double min,
			double max) throws Exception
	{
		DataRow dataRow;
		while ((dataRow = inData.poll()) != null)
		{

			// Get the cells
			DataCell targetCell = dataRow.getCell(targetColIndex);
			DataCell predictionCell = dataRow.getCell(predictionColIndex);
			DataCell valueCell = dataRow.getCell(valueColIndex);

			List<String> errors = new ArrayList<>();
			
			if (targetCell.isMissing() || valueCell.isMissing())
			{
				errors.add("Target or prediction value missing");
			}
			else if (!targetValueCompatible(((StringCell) targetCell).getStringValue())) 
			{
				errors.add("Target value not compatible with specified active and inactive strings");
			}
			else
			{
				try
				{
					double value = ((DoubleValue) valueCell).getDoubleValue();

					if (value < min)
						throw new IllegalArgumentException("Value falls below specified range");

					int binIndex = binner.findBinIndex(value);

					if (predictionCell.isMissing())
					{
						if (settingMissingValueAsOut.getBooleanValue())
						{
							streamResults.get(binIndex).increment(((StringValue) targetCell).getStringValue(), null);
						} else
						{
							errors.add("Target value is missing");
						}
					} else
					{
						streamResults.get(binIndex).increment(((StringValue) targetCell).getStringValue(),
								((StringValue) predictionCell).getStringValue());
					}
				} catch (Exception e)
				{
					errors.add(e.getMessage());
				}				
			}
			
			if(!errors.isEmpty())
				outError.push(createErrorRow(dataRow, errors));
		}
		inData.close();
		outError.close();
	}

}
