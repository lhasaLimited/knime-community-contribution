package org.lhasalimited.generic.stringdiff;

import java.util.LinkedList;

import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Diff;
import org.knime.core.data.DataCell;
import org.knime.core.data.DataColumnSpec;
import org.knime.core.data.DataColumnSpecCreator;
import org.knime.core.data.DataRow;
import org.knime.core.data.DataTableSpec;
import org.knime.core.data.MissingCell;
import org.knime.core.data.StringValue;
import org.knime.core.data.container.AbstractCellFactory;
import org.knime.core.data.container.ColumnRearranger;
import org.knime.core.data.def.BooleanCell.BooleanCellFactory;
import org.knime.core.data.def.DoubleCell.DoubleCellFactory;
import org.knime.core.data.def.IntCell.IntCellFactory;
import org.knime.core.data.html.HTMLCellFactory;
import org.knime.core.node.InvalidSettingsException;
import org.lhasalimited.generic.node.config.StreamableLocalSettingsNodeModel;

/**
 * <code>NodeModel</code> for the "StringComparison" node.
 *
 * @author Lhasa Limited
 */
public class StringComparisonNodeModel extends StreamableLocalSettingsNodeModel<StringComparisonNodeSettings>
{

	private final int NUM_OUT_CELL = 7;
	
	@Override
	protected ColumnRearranger createColumnRearrangerImplementation(DataTableSpec spec)
			throws InvalidSettingsException
	{
		ColumnRearranger rearranger = new ColumnRearranger(spec);

		rearranger.append(new StringComparisonCellFactory(spec));

		return rearranger;
	}

	private boolean inputIsValid(DataCell... cells)
	{

		for (DataCell cell : cells)
		{
			if (cell.isMissing())
				return false;
		}

		return true;
	}

	private DataColumnSpec[] createNewColumnSpecs(DataTableSpec inSpec)
	{
		DataColumnSpec diffColSpec = (new DataColumnSpecCreator(DataTableSpec.getUniqueColumnName(inSpec, "String Comparison Result"), 
				HTMLCellFactory.TYPE).createSpec());
		DataColumnSpec isEqualColSpec = (new DataColumnSpecCreator(
				DataTableSpec.getUniqueColumnName(inSpec, "Is Equal"), BooleanCellFactory.TYPE).createSpec());
		DataColumnSpec hasWhiteSpaceDiffColSpec = (new DataColumnSpecCreator(
				DataTableSpec.getUniqueColumnName(inSpec, "Is equal ignoring white space"), BooleanCellFactory.TYPE)
						.createSpec());
		DataColumnSpec diffCountEqual = (new DataColumnSpecCreator(
				DataTableSpec.getUniqueColumnName(inSpec, "Equal Count"), IntCellFactory.TYPE).createSpec());
		DataColumnSpec diffCountInsert = (new DataColumnSpecCreator(
				DataTableSpec.getUniqueColumnName(inSpec, "Insertion Count"), IntCellFactory.TYPE).createSpec());
		DataColumnSpec diffCountDelete = (new DataColumnSpecCreator(
				DataTableSpec.getUniqueColumnName(inSpec, "Deletion Count"), IntCellFactory.TYPE).createSpec());
		DataColumnSpec percentageChange = (new DataColumnSpecCreator(
				DataTableSpec.getUniqueColumnName(inSpec, "% change"), DoubleCellFactory.TYPE).createSpec());

		return new DataColumnSpec[] { diffColSpec, isEqualColSpec, hasWhiteSpaceDiffColSpec, diffCountEqual,
				diffCountInsert, diffCountDelete, percentageChange };
	}
	
	private DataCell[] errorCells(int count)
	{
		DataCell[] cells = new DataCell[count];
		
		for(int i = 0; i < count; i++)
			cells[i] = new MissingCell("Input string cell for comparison is missing");;
		
		return cells;
	}

	class StringComparisonCellFactory extends AbstractCellFactory
	{
		int firstColumnIndex = -1;
		int secondColumnIndex = -1;
		private StringDiffUtils diffUtils;

		protected StringComparisonCellFactory(DataTableSpec inSpec)
		{
			super(true, createNewColumnSpecs(inSpec));

			firstColumnIndex = inSpec.findColumnIndex(localSettings.getColumnOneName());
			secondColumnIndex = inSpec.findColumnIndex(localSettings.getColumnTwoName());
			diffUtils = new StringDiffUtils(localSettings.isRetainWhiteSpaceBreaks(), false);
		}

		@Override
		public DataCell[] getCells(DataRow row)
		{
			DataCell[] outCells = new DataCell[NUM_OUT_CELL];

			if (!inputIsValid(row.getCell(firstColumnIndex), row.getCell(secondColumnIndex)))
			{
				outCells = errorCells(NUM_OUT_CELL);
			}
			else
			{
				String firstValue = ((StringValue) row.getCell(firstColumnIndex)).getStringValue();
				String secondValue = ((StringValue) row.getCell(secondColumnIndex)).getStringValue();

				LinkedList<Diff> diff = diffUtils.diff(firstValue, secondValue);
				diffUtils.postProcess(diff, localSettings.getPostProcessingSelection());
				
				DifferenceCounts counts = diffUtils.getCountsForOperations(diff);

				outCells[0] = HTMLCellFactory.INSTANCE.createCell(diffUtils.getHtmlFormattedDiff(diff));
				outCells[1] = BooleanCellFactory.create(row.getCell(firstColumnIndex).equals(row.getCell(secondColumnIndex)));
				outCells[2] = BooleanCellFactory.create(diffUtils.equalsIgnoringWhiteSpace(firstValue, secondValue));
				outCells[3] = IntCellFactory.create(counts.getEqualCharacters());
				outCells[4] = IntCellFactory.create(counts.getInsertedCharacters());
				outCells[5] = IntCellFactory.create(counts.getDeletedCharacters());
				outCells[6] = DoubleCellFactory.create(counts.getPercentageCharacterChange());
			}

			return outCells;
		}
	}

}
