package org.lhasalimited.generic.stringdiff;

import org.knime.core.node.NodeView;

/**
 * <code>NodeView</code> for the "StringComparison" node.
 *
 * @author Lhasa Limited
 */
public class StringComparisonNodeView extends NodeView<StringComparisonNodeModel>
{

	/**
	 * Creates a new view.
	 * 
	 * @param nodeModel The model (class: {@link StringComparisonNodeModel})
	 */
	protected StringComparisonNodeView(final StringComparisonNodeModel nodeModel)
	{
		super(nodeModel);
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void modelChanged()
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onClose()
	{
		// TODO: generated method stub
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void onOpen()
	{
		// TODO: generated method stub
	}

}
