package org.lhasalimited.generic.stringdiff;

public class DifferenceCounts
{
	
	private int insertCount;
	private int deleteCount;
	private int equalCount;
	
	private int insertedCharacters;
	private int deletedCharacters;
	private int equalCharacters;

	protected DifferenceCounts()
	{
		// Nothing to do
	}

	public int getInsertCount()
	{
		return insertCount;
	}

	public int getDeleteCount()
	{
		return deleteCount;
	}
	
	public int getEqualCount()
	{
		return equalCount;
	}
	
	
	public int getInsertedCharacters()
	{
		return insertedCharacters;
	}

	public int getDeletedCharacters()
	{
		return deletedCharacters;
	}
	
	public int getEqualCharacters()
	{
		return equalCharacters;
	}


	public void incrementInsert(int charactersChanged)
	{
		this.insertedCharacters += charactersChanged;
		insertCount++;
	}

	public void incrementDelete(int charactersChanged)
	{
		this.deletedCharacters += charactersChanged;
		deleteCount++;
	}
	
	public void incrementEqual(int charactersChanged)
	{
		this.equalCharacters += charactersChanged;
		equalCount++;
	}

	public double getPercentageCharacterChange()
	{
		return 1 - ((double) equalCharacters) / (insertedCharacters + deletedCharacters + equalCharacters);
	}
}

/* ---------------------------------------------------------------------*
 * This software is the confidential and proprietary
 * information of Lhasa Limited
 * Granary Wharf House, 2 Canal Wharf, Leeds, LS11 5PS
 * ---
 * No part of this confidential information shall be disclosed
 * and it shall be used only in accordance with the terms of a
 * written license agreement entered into by holder of the information
 * with LHASA Ltd.
 * --------------------------------------------------------------------- */