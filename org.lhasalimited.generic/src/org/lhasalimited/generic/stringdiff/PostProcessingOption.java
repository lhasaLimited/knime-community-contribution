package org.lhasalimited.generic.stringdiff;

public enum PostProcessingOption
{
	NONE, SEMANTIC, EFFICIENCY, SEMANTIC_LOSSLESS;
}
