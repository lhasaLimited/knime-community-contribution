package org.lhasalimited.generic.stringdiff;

import org.knime.core.node.defaultnodesettings.DefaultNodeSettingsPane;

/**
 * <code>NodeDialog</code> for the "StringComparison" node.
 * 
 * @author Lhasa Limited
 */
public class StringComparisonNodeDialog extends DefaultNodeSettingsPane 
{
	
	StringComparisonNodeSettings settings = new StringComparisonNodeSettings();
	
    /**
     * New pane for configuring the StringComparison node.
     */
    protected StringComparisonNodeDialog() 
    {
    	
    	addDialogComponent(settings.createDialogComponentColumnOne());
    	addDialogComponent(settings.createDialogComponentColumnTwo());
    	addDialogComponent(settings.createDialogComponentRetainWhiteSpace());
    	addDialogComponent(settings.createDialogComponentPostProcessing());
    }
}

