package org.lhasalimited.generic.stringdiff;

import java.util.Arrays;
import java.util.List;

import org.knime.base.node.io.variablecreator.SettingsModelVariables;
import org.knime.core.data.StringValue;
import org.knime.core.node.defaultnodesettings.DialogComponent;
import org.knime.core.node.defaultnodesettings.DialogComponentStringListSelection;
import org.knime.core.node.defaultnodesettings.SettingsModelBoolean;
import org.knime.core.node.defaultnodesettings.SettingsModelColumnName;
import org.knime.core.node.defaultnodesettings.SettingsModelString;
import org.lhasalimited.generic.node.config.NodeSettingCollection;

public class StringComparisonNodeSettings extends NodeSettingCollection
{

	public static final String CFG_COLUMN_ONE_NAME = "cfgColumnOneName";
	public static final String CFG_COLUMN_TWO_NAME = "cfgColumnTwoName";
	public static final String CFG_RETAIN_WHITE_SPACE = "cfgRetainWhiteSpace";
	public static final String CFG_POST_PROCESSING = "cfgPostProcessing";
	
	
	@Override
	protected void addSettings()
	{
		addSetting(CFG_COLUMN_ONE_NAME, new SettingsModelColumnName(CFG_COLUMN_ONE_NAME, "First String Column"));
		addSetting(CFG_COLUMN_TWO_NAME, new SettingsModelColumnName(CFG_COLUMN_TWO_NAME, "Second String Column"));
		addSetting(CFG_RETAIN_WHITE_SPACE, new SettingsModelBoolean(CFG_RETAIN_WHITE_SPACE, true));
		addSetting(CFG_POST_PROCESSING, new SettingsModelString(CFG_POST_PROCESSING, "NONE"));
	}

	public boolean isRetainWhiteSpaceBreaks()
	{
		return getBooleanValue(CFG_RETAIN_WHITE_SPACE);
	}
	
	public String getColumnOneName()
	{
		return getColumnName(CFG_COLUMN_ONE_NAME);
	}

	public String getColumnTwoName()
	{
		return getColumnName(CFG_COLUMN_TWO_NAME);
	}
	
	public PostProcessingOption getPostProcessingSelection()
	{
		return PostProcessingOption.valueOf((getString(CFG_POST_PROCESSING)));
	}

	public DialogComponent createDialogComponentColumnOne()
	{
		return getDialogColumnNameSelection(CFG_COLUMN_ONE_NAME, "First Column String", 0, StringValue.class);
	}

	public DialogComponent createDialogComponentColumnTwo()
	{
		return getDialogColumnNameSelection(CFG_COLUMN_TWO_NAME, "Second Column String", 0, StringValue.class);
	}
	
	public DialogComponent createDialogComponentRetainWhiteSpace()
	{
		return getDialogComponentBoolean(CFG_RETAIN_WHITE_SPACE, "Retain white space");
	}
	
	public DialogComponent createDialogComponentPostProcessing()
	{
		return getDialogComponentDropdownList(CFG_POST_PROCESSING, "Cleanup Method", List.of(PostProcessingOption.NONE.toString(),
																					PostProcessingOption.EFFICIENCY.toString(),
																					PostProcessingOption.SEMANTIC.toString(),
																					PostProcessingOption.SEMANTIC_LOSSLESS.toString()));
	}

}

/* ---------------------------------------------------------------------*
 * This software is the confidential and proprietary
 * information of Lhasa Limited
 * Granary Wharf House, 2 Canal Wharf, Leeds, LS11 5PS
 * ---
 * No part of this confidential information shall be disclosed
 * and it shall be used only in accordance with the terms of a
 * written license agreement entered into by holder of the information
 * with LHASA Ltd.
 * --------------------------------------------------------------------- */