package org.lhasalimited.generic.stringdiff;

import java.util.LinkedList;

import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Diff;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Operation;
import org.jsoup.nodes.Entities;

public class StringDiffUtils
{

	private boolean retainWhiteSpaceInHtmlOutput = true;
	private boolean wrapInHtmlAndBodyTags = false;
	private DiffMatchPatch dmp;

	public StringDiffUtils()
	{
		this(true, false);
	}
	
	public StringDiffUtils(boolean retainWhiteSpaceInHtmlOutput, boolean wrapInHtmlAndBodyTags)
	{
		this.retainWhiteSpaceInHtmlOutput = retainWhiteSpaceInHtmlOutput;
		this.wrapInHtmlAndBodyTags = wrapInHtmlAndBodyTags;
		this.dmp = new DiffMatchPatch();
	}

	/**
	 * Generates the diff result from `DiffMatchPatch`
	 */
	public LinkedList<Diff> diff(String firstString, String secondString)
	{
		return dmp.diffMain(firstString, secondString, false);
	}

	/**
	 * Formats the `DiffMatchPatch` diff result in the pretty html as provided by
	 * the library
	 */
	public String getDiffPrettyHtml(LinkedList<Diff> diff)
	{
		return dmp.diffPrettyHtml(diff);
	}

	/**
	 * Generates the `DiffMatchPatch` diff and formats the result in custom HTMl
	 * designed for rendering in the KNIME table
	 */
	public String getHtmlFormattedDiff(String firstString, String secondString)
	{
		return getHtmlFormattedDiff(diff(firstString, secondString));
	}

	/**
	 * Formats the `DiffMatchPatch` diff result in custom HTMl designed for
	 * rendering in the KNIME table
	 */
	public String getHtmlFormattedDiff(LinkedList<Diff> diff)
	{
		StringBuilder htmlStringBuilder = new StringBuilder();

		if(wrapInHtmlAndBodyTags)
			htmlStringBuilder.append("<html>\r\n").append("<body>\r\n");

		for (Diff item : diff)
		{
			String htmlEscapedItemText = Entities.escape(item.text);

			if (retainWhiteSpaceInHtmlOutput)
			{
				htmlEscapedItemText = htmlEscapedItemText.replaceAll("(\r\n|\n)", "<br>");
				htmlEscapedItemText = htmlEscapedItemText.replaceAll(" ", "&nbsp;");
			}
			
			if (item.operation == Operation.EQUAL)
			{
				htmlStringBuilder.append(htmlEscapedItemText);
			}
			else
			{
				htmlStringBuilder
					.append("<span style=\"background-color: ")
					.append(getColourForOperation(item.operation))
					.append("\">")
					.append(htmlEscapedItemText)
					.append("</span>");
			}
		}

		if(wrapInHtmlAndBodyTags)
			htmlStringBuilder.append("</body>\r\n").append("</html>");
		
		return htmlStringBuilder.toString();
	}

	private String getColourForOperation(Operation operation)
	{
		String colour = null;

		switch (operation)
		{
			case DELETE:
				colour = "#FA8686";
				break;
			case INSERT:
				colour = "#88FA86";
				break;

			default:
				break;
		}

		return colour;
	}

	public DifferenceCounts getCountsForOperations(LinkedList<Diff> diff)
	{

		DifferenceCounts counts = new DifferenceCounts();

		for (Diff section : diff)
		{
			switch (section.operation)
			{

				case EQUAL:
					counts.incrementEqual(section.text.length());
					break;
				case INSERT:
					counts.incrementInsert(section.text.length());
					break;
				case DELETE:
					counts.incrementDelete(section.text.length());
					break;

				default:
					break;
			}
		}

		return counts;
	}

	public boolean equalsIgnoringWhiteSpace(String firstString, String secondString)
	{
		return firstString.replaceAll("\\s+","").equals(secondString.replaceAll("\\s+",""));
	}
	
	public void postProcess(LinkedList<Diff> diff, PostProcessingOption option)
	{
		switch (option)
		{
			case NONE:
				break;
			case EFFICIENCY:
				dmp.diffCleanupEfficiency(diff);
				break;
			case SEMANTIC:
				dmp.diffCleanupSemantic(diff);
				break;
			case SEMANTIC_LOSSLESS:
				dmp.diffCleanupSemanticLossless(diff);
				break;
		}
	}
}
